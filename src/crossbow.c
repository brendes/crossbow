/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include "config.h"
#include "help_helper.h"

#define PROGNAME "crossbow"

static void help(void)
{
    show_help(
        help_usage,
        &(struct help){
            .progname = PROGNAME,
            .args = "<command> [...]",
        },
        NULL
    );
}

static void version(void)
{
    fputs(PACKAGE_NAME " version " PACKAGE_VERSION "\n", stderr);
    fputs("bug reports: " PACKAGE_BUGREPORT "\n", stderr);
    exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
    char command_name[32] = PROGNAME "-";

    if (argc < 2)
        help();

    if (strcmp(argv[1], "--version") == 0)
        version();

    if (argv[1][0] == '-')
        help();

#ifdef HAVE_PLEDGE
    if (pledge("stdio exec", NULL) == -1)
        err(1, "pledge");
#endif

    command_name[sizeof(command_name) - 1] = 0;
    strncpy(
        command_name + sizeof(PROGNAME),
        argv[1],
        sizeof(command_name) - 1 - sizeof(PROGNAME)
    );

    argv[1] = command_name;
    execvp(command_name, argv + 1);
    err(EX_OSERR, "exec (\"%s\") failed", command_name);
}
