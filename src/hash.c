/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "hash.h"

#include <err.h>
#include <errno.h>
#include <stdlib.h>

#include <uthash.h>

enum
{
    SET_INITIAL_SIZE = 16,
    SET_GROW_FACTOR = 2,
};

struct item
{
    str_t key;
    void *value;
    struct UT_hash_handle hh;
};

bool hash_contains(const hash_t *items_set,
                   const str_t *key)
{
    struct item *found;

    HASH_FIND(hh, items_set->head, key->bytes, key->len, found);
    return !!found;
}

int hash_insert(hash_t *items_set, str_t *key, void *value)
{
    struct item *item;

    HASH_FIND(hh, items_set->head, key->bytes, key->len, item);
    if (item) {
        item->value = value;
        return 0;
    }

    item = malloc(sizeof(struct item));
    if (!item) {
        warn("malloc failed");
        return -1;
    }

    *item = (struct item){
        .key = {},
        .value = value,
    };
    str_move(&item->key, key);
    HASH_ADD(hh, items_set->head, key.bytes[0], item->key.len, item);
    return 0;
}

hash_pair_t hash_iter(const hash_t *items_set, void **aux)
{
    struct item *item = *aux;

    if (item)
        item = item->hh.next;
    else
        item = items_set->head;

    if (!item)
        return (hash_pair_t){};

    *aux = item;
    return (hash_pair_t){ .key = &item->key, .value = item->value };
}

void hash_free(hash_t *items_set)
{
    struct item *item, *aux;

    HASH_ITER(hh, items_set->head, item, aux) {
        HASH_DEL(items_set->head, item);
        str_free(&item->key);
        free(item);
    }
}
