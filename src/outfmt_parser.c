/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "outfmt_parser.h"

#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>

typedef enum {
    symbol_class_invalid = -1,
    symbol_class_escape,
    symbol_class_pholder,
    symbol_class_verbatim,
    symbol_class_whitespace,
} symbol_class_t;

static symbol_class_t get_symbol_class(char c)
{
    switch (c) {
    case '\\':
        return symbol_class_escape;
    case '%':
        return symbol_class_pholder;
    default:
        return isspace(c) ? symbol_class_whitespace
             : isprint(c) ? symbol_class_verbatim
             : symbol_class_invalid;
    }
}

static ofp_atom_t atom_of(symbol_class_t sycl)
{
    switch (sycl) {
    case symbol_class_escape:
        return ofp_atom_verbatim;
    case symbol_class_pholder:
        return ofp_atom_pholder;
    case symbol_class_verbatim:
        return ofp_atom_verbatim;
    case symbol_class_whitespace:
        return ofp_atom_whitespace;
    case symbol_class_invalid:
        break;
    }
    abort();
}

static inline int fail(ofp_setup_t ofps, ofp_fail_t how)
{
    assert(how != ofp_fail_bad_state);   /* algorithm is buggy */
    ofps->fail(ofps->opaque, how);
    return -1;
}

static inline int try_emit(ofp_setup_t ofps,
                           ofp_atom_t atom,
                           const char *begin,
                           size_t len)
{

    if (len == 0)
        return 0;

    if (atom == ofp_atom_pholder && len < 2)
        return fail(ofps, ofp_fail_invalid_pholder);

    int ans = ofps->emit(
        ofps->opaque,
        &(ofp_token_t){
            .atom = atom,
            .begin = begin,
            .len = len,
        }
    );

    if (ans < 0 || ans > len)
        return fail(ofps, ofp_fail_emitting);

    return ans;
}

const char * ofp_atom_str(ofp_atom_t a)
{
    static const char * const repr[] = {
        "ofp_atom_pholder",
        "ofp_atom_verbatim",
        "ofp_atom_whitespace",
    };
    return repr[a];
}

const char * ofp_fail_str(ofp_fail_t f)
{
    static const char * const repr[] = {
        [ofp_fail_none]                 = "ofp_fail_none",
        [ofp_fail_bad_spec]             = "ofp_fail_bad_spec",
        [ofp_fail_bad_state]            = "ofp_fail_bad_state",
        [ofp_fail_emitting]             = "ofp_fail_emitting",
        [ofp_fail_invalid_pholder]      = "ofp_fail_invalid_pholder",
        [ofp_fail_trail_escape]         = "ofp_fail_trail_escape",
    };
    return repr[f];
}

int ofp_scan(ofp_setup_t ofps, const char *spec, size_t speclen)
{
    if (speclen == 0 || speclen > INT_MAX)
        return fail(ofps, ofp_fail_bad_spec);

    int escaped = 0;
    const char *begin = NULL;
    ofp_atom_t atom;

    static const symbol_class_t stay_if[] = {
        [ofp_atom_pholder]    = symbol_class_verbatim,
        [ofp_atom_verbatim]   = symbol_class_verbatim,
        [ofp_atom_whitespace] = symbol_class_whitespace,
    };

    for (int i = 0; i <= speclen; ++i) {

        if (i == speclen) {
            if (!begin)
                continue;   /* nothing left, actual end. */

            if (escaped)
                return fail(ofps, ofp_fail_trail_escape);

            /* continuing for a last 'emit' with what is left,
             * possibly with a rewind. */
        } else {
            symbol_class_t sycl;

            if (escaped) {
                --escaped;
                sycl = symbol_class_verbatim;
            } else {
                sycl = get_symbol_class(spec[i]);
                if (sycl == symbol_class_invalid)
                    return fail(ofps, ofp_fail_bad_spec);
                if (sycl == symbol_class_escape) {
                    escaped = 1;
                    sycl = symbol_class_verbatim;
                }
            }

            if (!begin) {
                begin = spec + i;
                atom = atom_of(sycl);
                continue;
            }

            if (stay_if[atom] == sycl)
                continue;
        }

        int len = spec + i - begin;
        int emit_r = try_emit(ofps, atom, begin, len);
        if (emit_r < 0)
            return emit_r;

        if (emit_r)
            i -= len - emit_r;
        begin = NULL;
        --i;
        if (escaped)
            ++escaped;  /* due to re-evaluation */
    }

    return 0;
}
