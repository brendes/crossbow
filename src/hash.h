/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "str.h"

typedef struct {
    struct item *head;
} hash_t;

#define HASH_INIT (hash_t){}

bool hash_contains(const hash_t *, const str_t *key);

int hash_insert(hash_t *, str_t *key, void *value);

typedef struct {
    const str_t *key;
    void *value;
} hash_pair_t;

hash_pair_t hash_iter(const hash_t *, void **aux);

void hash_free(hash_t *);
