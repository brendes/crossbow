#pragma once

#include <stddef.h>

const char *text_short(char *dst, size_t dst_size,
                       const char *text,
                       const size_t *opt_text_len);


const char *text_join_argv(char *dst, size_t dst_size,
                           const char * const *argv);
