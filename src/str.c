/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "str.h"

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>

void str_assign(str_t *str,
                const char *bytes,
                unsigned len,
                bool owned)
{
    str_free(str);
    if (len) {
        str->bytes = bytes;
        str->len = len;
        str->owned = owned;
    }
}

void str_move(str_t *dst, str_t *src)
{
    str_free(dst);
    *dst = *src;
    src->owned = false;
}

int str_dup(str_t *dst, const str_t *src)
{
    const char *copy;

    if (src->len == 0) {
        /* The source string is empty, so will be the
         * destination string. */
        str_free(dst);
        return 0;
    }

    copy = strndup(src->bytes, src->len);
    if (!copy) {
        warn("strndup failed");
        return -1;
    }

    str_assign(dst, copy, src->len, true);
    return 0;
}

int str_cmp(const str_t *s1, const str_t *s2)
{
    if (s1->len == 0)
        return s2->len == 0 ? 0 : -1;

    if (s2->len == 0)
        return 1;

    int v = strncmp(
        s1->bytes,
        s2->bytes,
        s1->len < s2->len ? s1->len : s2->len
    );

    if (v || s1->len == s2->len)
        return v;

    return s1->len < s2->len ? -1 : 1;
}

int str_cmp_up_to(const str_t *s1, const str_t *s2, unsigned len)
{
    return str_cmp(
        &(str_t){
            .bytes = s1->bytes,
            .len = s1->len < len ? s1->len : len
        },
        &(str_t){
            .bytes = s2->bytes,
            .len = s2->len < len ? s2->len : len
        }
    );
}

str_t str_sub(const str_t *str, unsigned start, unsigned len)
{
    if (start >= str->len || len == 0)
        return (str_t){};

    str_t out = {
        .bytes = str->bytes + start,
        .len = str->len - start,
    };

    if (len < out.len)
        out.len = len;

    return out;
}

const char *str_to_cstr(const str_t *str, char *buffer, size_t len)
{
    if (str->len > len - 1)
        return NULL;

    memcpy(buffer, str->bytes, str->len);
    buffer[str->len] = '\0';
    return buffer;
}

void str_free(str_t *str)
{
    if (!str)
        return;

    if (str->owned)
        free((void *)str->bytes);

    str->bytes = NULL;
    str->len = 0;
    str->owned = false;
}
