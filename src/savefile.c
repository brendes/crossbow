/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "logging.h"
#include "savefile.h"
#include "str.h"
#include "test_counters.h"

#include <assert.h>
#include <dirent.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <unistd.h>

#include <uthash.h>

enum
{
    feed_name_offset = 1,
};

typedef enum entry_kind_t entry_kind_t;
typedef enum file_status_t file_status_t;
typedef enum flag_idx_t flag_idx_t;

typedef struct revision_t revision_t;
typedef struct filemap_t filemap_t;
typedef struct err_t err_t;
typedef struct feed_t feed_t;
typedef struct entry_t entry_t;

/* this enueration matters for backward compatibility */
enum entry_kind_t
{
    ek_none = 0,
    ek_feed_item,
    ek_url_provided,
    ek_outfmt,
    ek_flags,
    ek_url_effective,
    ek_subproc_chdir,
    ek_items_count,
};

enum file_status_t
{
    fs_unknown = 0,
    fs_noent,           // file does not exist yet
    fs_regular,         // file exists
    fs_unrecognized,    // file exists, format not recognized
    fs_incompat,        // file exists, recognized, different version
    fs_corrupt,         // file exists, recognized, same version, corrupt
};

/* this enueration matters for backward compatibility */
enum flag_idx_t
{
    fi_output_mode,
    fi_url_type,
    fi_flags_length,    // guard
};

struct revision_t
{
    uint8_t major;
    uint8_t minor;
} __attribute__((packed, aligned(1)));

struct filemap_t
{
    void *memory;
    size_t size;
};

struct err_t
{
    /*  >= 0    errno_val = -value  error = sfe_system_error
     *  < 0     errno_val = 0       error = value
     */
    int value;
};

struct feed_t
{
    UT_hash_handle hh;
    const char *name;
    filemap_t filemap;
    struct {
        str_t provided;
        str_t effective;
    } url;
    str_t outfmt;
    str_t subproc_chdir;
    str_t items[feed_max_items];
    file_status_t file_status;
    unsigned n_items;
    unsigned items_count;
    int dirfd;          /* not owned. */
    uint8_t flags[fi_flags_length];
    bool marked_for_deletion : 1;
    bool needs_write : 1;
};

struct savefile_t
{
    int dirfd;
    err_t last_error;
    feed_t *feeds;
    bool basedir_scanned : 1;
    bool feed_addition : 1;
};

struct entry_t
{
    uint8_t kind;
    uint16_t length;
} __attribute__((packed, aligned(1)));

const char *file_status_str[] = {
    [fs_unknown]        = "fs_unknown",
    [fs_noent]          = "fs_noent",
    [fs_regular]        = "fs_regular",
    [fs_unrecognized]   = "fs_unrecognized",
    [fs_incompat]       = "fs_incompat",
    [fs_corrupt]        = "fs_corrupt",
};

static const char *dotstrdup(const char *);
static int writev_wrap(int fd, struct iovec *, size_t);
static int filesize(int fd, size_t *s);

static int filemap_load(filemap_t *, int fd);
static void filemap_unload(filemap_t *);

static err_t err_set(int enum_value);
static savefile_err_t err_get(err_t);

static int entry_write(int fd, const str_t *entry, entry_kind_t);
static int entry_load(feed_t *, int offset, str_t *, entry_kind_t *);

static feed_t *feed_new(int dirfd, const char *fname);
static const char * feed_name(const feed_t *);
static bool feed_can_be_written(const feed_t *);
static int feed_load_magic(feed_t *);
static void feed_load_items_count(feed_t *, const str_t *);
static int feed_load(feed_t *, int fd);
static int feed_write_magic(int fd);
static int feed_write_fields(const feed_t *, int fd);
static int feed_write_items_count(const feed_t *, int fd);
static int feed_write(feed_t *);
static int feed_unlink(feed_t *);
static void feed_set_str_field(feed_t *f, str_t *field, str_t *new_value);
static void feed_set_flag(feed_t *f, flag_idx_t idx, uint8_t byte);
static void feed_flag_to_write(feed_t *f);
static void feed_free_fields(feed_t *);
static void feed_free(feed_t *);
static int feed_add_item(feed_t *, str_t *);

static feed_t *save_find_feed(savefile_t *, const char *fname);
static bool save_is_bogus_fname(const char *fname);
static feed_t *save_add_feed(savefile_t *save, const char *fname);
static int save_scan_feeds(savefile_t *);

#define magic_nr "crossbow"
static const revision_t magic_revision = {
    .major = 0,
    .minor = 0,
};


static const char *dotstrdup(const char * str)
{
    size_t n;
    char *out;

    n = 2 + strlen(str);
    out = malloc(n);
    if (!out) {
        warn("malloc failed");
        return NULL;
    }

    out[0] = '.';
    strcpy(out + feed_name_offset, str);
    out[n - 1] = 0;
    return out;
}

static int writev_wrap(int fd, struct iovec *iovec, size_t size)
{
    /* note, n is the *total* size of the array!  This is for the
     * convenience of not doing the sizeof/sizeof dance every time.  */

    if (writev(fd, iovec, size / sizeof(*iovec)) >= 0)
        return 0;

    warn("writev failed");
    return -1;
}

static int filesize(int fd, size_t *s)
{
    struct stat sb;

    if (fstat(fd, &sb) == -1) {
        warn("fstat failed");
        return -1;
    }

    *s = sb.st_size;
    return 0;
}

static int filemap_load(filemap_t *fmap, int fd)
{
    size_t size;
    void *mapped;

    if (filesize(fd, &size) == -1)
        return -1;

    *fmap = (filemap_t) {};
    if (size == 0)
        /* An empty file is not an error.  It is handled as there
         * was no file in the first place */
        goto success;

    mapped = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (mapped == MAP_FAILED) {
        warn("mmap failed");
        return -1;
    }

    *fmap = (filemap_t) {
        .memory = mapped,
        .size = size,
    };

success:
    close(fd);
    return 0;
}

static void filemap_unload(filemap_t *fmap)
{
    if (fmap->memory == MAP_FAILED || !fmap->memory)
        return;

    munmap(fmap->memory, fmap->size);
    *fmap = (filemap_t) {};
}

static err_t err_set(int enum_value)
{
    return (err_t){
        .value = (enum_value == sfe_system_error) ? -errno : enum_value
    };
}

static savefile_err_t err_get(err_t err)
{
    if (err.value >= 0)
        return (savefile_err_t){
            .error = err.value
        };

    return (savefile_err_t){
        .error = sfe_system_error,
        .errno_val = -err.value,
    };
}

static int entry_write(int fd, const str_t *str, entry_kind_t ek)
{
    entry_t entry = {
        .kind = ek,
        .length = str->len,
    };
    struct iovec iovec[2] = {
        [0].iov_base = &entry,
        [0].iov_len = sizeof(entry),
        [1].iov_base = (void *)str->bytes,
        [1].iov_len = str->len,
    };

    return writev_wrap(fd, iovec, sizeof(iovec));
}

static int flag_corrupt(feed_t *feed)
{
    warnx("feed [%s]: file too short", feed_name(feed));
    test_counter(flag_corrupt);
    feed->file_status = fs_corrupt;
    return 0;
}

static int entry_load(feed_t *feed,
                      int read_offset,
                      str_t *str,
                      entry_kind_t *ek)
{
    entry_t *entry;

    if (sizeof(entry_t) > feed->filemap.size - read_offset)
        return flag_corrupt(feed);

    entry = (entry_t *)((uint8_t *)feed->filemap.memory + read_offset);
    read_offset += sizeof(entry_t);

    if (entry->length > feed->filemap.size - read_offset)
        return flag_corrupt(feed);

    *ek = entry->kind;
    *str = (str_t){
        .bytes = (const char *)feed->filemap.memory + read_offset,
        .len = entry->length,
    };
    read_offset += entry->length;

    return read_offset;
}

static feed_t *feed_new(int dirfd, const char *fname)
{
    feed_t *feed = NULL;
    int fd;

    /* We try to open the feed file.   It might not exist, but this is not
     * a problem, as we might create one later.  We fail in case openat
     * fails for reason other than ENOENT.
     */
    fd = openat(dirfd, fname, O_RDONLY | O_CLOEXEC);
    if (fd == -1 && errno != ENOENT) {
        warn("openat(%d, %s, O_RDONLY)", dirfd, fname);
        goto fail;
    }

    feed = malloc(sizeof(feed_t));
    if (!feed) {
        warn("malloc failed");
        goto fail;
    }

    *feed = (feed_t) {
        .name = dotstrdup(fname),
        .dirfd = dirfd,
    };

    if (!feed->name)
        goto fail;

    if (fd == -1)
        feed->file_status = fs_noent;
    else {
        if (feed_load(feed, fd) == -1)
            goto fail;

        /* A corrupt file can be used and written over, but we have to be
         * sure to get rid of what we managed to load from the corrupt
         * file. */
        if (feed->file_status == fs_corrupt) {
            feed_free_fields(feed);
            filemap_unload(&feed->filemap);
        }
    }

    return feed;

fail:
    feed_free(feed);
    if (fd != -1)
        close(fd);
    return NULL;
}

static const char * feed_name(const feed_t *feed)
{
    return feed->name + feed_name_offset;
}

static bool feed_can_be_written(const feed_t *feed)
{
    switch (feed->file_status) {
    case fs_unknown:
        assert(false);  // is only used during initialization
    case fs_noent:
    case fs_regular:
    case fs_corrupt:
        return true;
    case fs_unrecognized:
    case fs_incompat:
        return false;
    }
    abort();
}

static int feed_load_magic(feed_t *feed)
{
    /* Check if the magic number exists.
     *
     * returns -1 on failure (it is expected by contract, but it never
     *      happens in reality.
     * returns 0 if the file is unrecognized.
     * returns the offset of the first byte beyond the magic number if the
     *      file is recognized.
     */

    if (feed->filemap.size < sizeof(magic_nr) + sizeof(revision_t)) {
        whisper("feed [%s]: no magic number", feed_name(feed));
        goto unrecognized;
    }

    const uint8_t *bytes = feed->filemap.memory;

    if (strncmp(magic_nr, (const char *)bytes, sizeof(magic_nr)) != 0) {
        whisper("feed [%s]: invalid magic number", feed_name(feed));
        goto unrecognized;
    }
    bytes += sizeof(magic_nr);

    revision_t *rev = (revision_t *)bytes;
    bytes += sizeof(revision_t);

    if (rev->major != magic_revision.major) {
        /* note - proper versioning is not supported yet */
        whisper("feed [%s]: unmatched magic version (%d)",
                feed_name(feed), rev->major);
        feed->file_status = fs_incompat;
        return 0;
    }

    return bytes - (uint8_t *)feed->filemap.memory;

unrecognized:
    feed->file_status = fs_unrecognized;
    return 0;
}

static void feed_load_items_count(feed_t *feed, const str_t *repr)
{
    if (repr->len != sizeof(feed->items_count)) {
        warnx("feed [%s]: unable to load items_count, "
              "assuming it is zero", feed_name(feed));
        feed->items_count = 0;
    } else
        memcpy(&feed->items_count, repr->bytes, sizeof(feed->items_count));
}

static int feed_load(feed_t *feed, int fd)
{
    /* This function is only invoked when constructing the feed.  Failing
     * here (return -1) implies that the 'feed' object will be destroyed,
     * therefore assigning 'last_error' is not required.  In failure
     * situations which are not related to system errors (e.g. corrupt
     * file) this function is going to be successful, but the
     * 'file_status' field will be set accordingly.
     */

    if (filemap_load(&feed->filemap, fd) == -1)
        return -1;

    int read_offset = feed_load_magic(feed);
    if (read_offset <= 0)
        return read_offset;

    while (read_offset < feed->filemap.size) {
        entry_kind_t ek = ek_none;
        int updated_offset;
        str_t content;

        updated_offset = entry_load(feed, read_offset, &content, &ek);
        if (updated_offset <= 0)
            return updated_offset;

        switch (ek) {
        case ek_feed_item:
            /* failure explicitly ignored */
            if (feed_add_item(feed, &content) == -1)
                goto done;
            break;

        case ek_url_provided:
            str_move(&feed->url.provided, &content);
            break;

        case ek_outfmt:
            str_move(&feed->outfmt, &content);
            break;

        case ek_flags: // not yet ready
            for (int i = 0; i < fi_flags_length; ++i)
                /* not using feed_set_flag on purpose! */
                feed->flags[i] = content.bytes[i];

            if (fi_flags_length < content.len) {
                whisper("feed [%s]: ignoring %d flags",
                        feed_name(feed),
                        content.len - fi_flags_length);
            }
            break;

        case ek_url_effective:
            str_move(&feed->url.effective, &content);
            break;

        case ek_subproc_chdir:
            str_move(&feed->subproc_chdir, &content);
            break;

        case ek_items_count:
            feed_load_items_count(feed, &content);
            break;

        case ek_none:
            /* Most likely a bug.  Probably in entry_load? */
            warnx("feed [%s]: reports entry of kind ek_none??",
                  feed_name(feed));
        default:
            whisper("feed [%s]: ignoring entry kind %d",
                    feed_name(feed), ek);
            break;
        }

        read_offset = updated_offset;
    }

done:
    feed->file_status = fs_regular;
    return 0;
}

static int feed_write_magic(int fd)
{
    static struct iovec iovec[] = {
        [0].iov_base = (void *)magic_nr,
        [0].iov_len = sizeof(magic_nr),
        [1].iov_base = (void *)&magic_revision,
        [1].iov_len = sizeof(revision_t),
    };
    return writev_wrap(fd, iovec, sizeof(iovec));
}

static int feed_write_fields(const feed_t *feed, int fd)
{
    for (int i = 0; feed->items[i].bytes; ++i)
        if (entry_write(fd, &feed->items[i], ek_feed_item) == -1)
            return -1;

    if (entry_write(fd, &feed->url.provided, ek_url_provided) == -1)
        return -1;

    if (entry_write(fd, &feed->outfmt, ek_outfmt) == -1)
        return -1;

    const str_t flags = {
        .bytes = (const char*)feed->flags,
        .len = sizeof(feed->flags)
    };

    if (entry_write(fd, &flags, ek_flags) == -1)
        return -1;

    if (entry_write(fd, &feed->url.effective, ek_url_effective) == -1)
        return -1;

    if (entry_write(fd, &feed->subproc_chdir, ek_subproc_chdir) == -1)
        return -1;

    if (feed_write_items_count(feed, fd) == -1)
        return -1;

    return 0;
}

static int feed_write_items_count(const feed_t *feed, int fd)
{
    entry_t entry = {
        .kind = ek_items_count,
        .length = sizeof(feed->items_count),
    };

    struct iovec iovec[2] = {
        [0].iov_base = &entry,
        [0].iov_len = sizeof(entry),
        [1].iov_base = (void *)&feed->items_count,
        [1].iov_len = sizeof(feed->items_count),
    };

    return writev_wrap(fd, iovec, sizeof(iovec));
}

static int feed_write(feed_t *feed)
{
    /* this function saves the feed on a file with the same name as the
     * original feed file, but prefixed with a dot.  If the write is
     * successful the file is renamed ro replace the original one.
     *
     * The feed->name field contains the name of the dot-prefixed file,
     * so feed->name + 1 points to a string that corresponds to the
     * original file name.  A little trick to spare some extra work.
     *
     * To make it it more explicit, feed_name_offset marks the use
     * of this memory optimization.
     */

    int fd = openat(feed->dirfd, feed->name,
        O_WRONLY | O_APPEND | O_CREAT | O_EXCL | O_CLOEXEC,
        S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP
    );

    if (fd == -1) {
        if (errno == EEXIST)
            warnx("auxiliary savefile '%s' exists: failed save attempt?",
                  feed->name);
        else
            warn("openat failed");
        return -1;
    }

    if (feed_write_magic(fd) == -1) {
        close(fd);
        return -1;
    }

    if (feed_write_fields(feed, fd) == -1) {
        close(fd);
        return -1;
    }

    if (fsync(fd) == -1) {
        warn("faulty flush of savefile");
        return -1;
    }

    if (close(fd) == -1) {
        warn("faulty close of (flushed) savefile");
        return -1;
    }

    if (renameat(feed->dirfd, feed->name, feed->dirfd, feed_name(feed)) == -1)
        return -1;

    test_counter(feed_written);
    feed->needs_write = false;
    return 0;
}

static int feed_unlink(feed_t *feed)
{
    const char *filename = feed_name(feed);

    if (unlinkat(feed->dirfd, filename, 0) == 0) {
        test_counter(feed_unlinked);
        feed_flag_to_write(feed);
        feed->marked_for_deletion = false;
        return 0;
    }

    warn("unlikat(%d, %s) failed", feed->dirfd, filename);
    return -1;
}

int feed_persist(feed_t *feed)
{
    if (!feed_can_be_written(feed)) {
        warnx(
            "NOTICE: feed [%s] cannot be written safely (%s), skipped",
            feed_name(feed),
            file_status_str[feed->file_status]
        );
    }

    if (feed->marked_for_deletion)
        return feed_unlink(feed);

    if (feed->needs_write)
        return feed_write(feed);

    return 0;
}

static void feed_set_str_field(feed_t *feed, str_t *field, str_t *new_value)
{
    if (str_cmp(field, new_value) == 0)
        return;

    str_move(field, new_value);
    feed_flag_to_write(feed);
}

static void feed_set_flag(feed_t *feed, flag_idx_t idx, uint8_t byte)
{
    if (feed->flags[idx] == byte)
        return;

    feed->flags[idx] = byte;
    feed_flag_to_write(feed);
}

static void feed_flag_to_write(feed_t *feed)
{
    test_counter(needs_write);
    feed->needs_write = true;
}

static void feed_clear_items(feed_t *feed)
{
    for (unsigned i = 0; i < feed->n_items; ++i)
        str_free(&feed->items[i]);
    feed->n_items = 0;
}

static void feed_free_fields(feed_t *feed)
{
    /* All the fields freed in this function have in common the fact that
     * they might be pointers to 'feed->filemap', unless differently
     * assigned by means of setters.
     */
    str_free(&feed->url.provided);
    str_free(&feed->url.effective);
    str_free(&feed->outfmt);
    str_free(&feed->subproc_chdir);
    feed_clear_items(feed);
}

static void feed_free(feed_t *feed)
{
    if (!feed)
        return;

    free((void *)feed->name);

    feed_free_fields(feed);
    filemap_unload(&feed->filemap);
    free(feed);
}

static int feed_add_item(feed_t *feed, str_t *data)
{
    if (feed->n_items >= feed_max_items) {
        warnx(
            "feed_add_item: maximum number of feed items (%d) exceeded",
            feed_max_items
        );
        return -1;
    }

    str_move(&feed->items[feed->n_items++], data);
    return 0;
}

static int save_scan_feeds(savefile_t *save)
{
    DIR *dir = NULL;
    struct dirent *dent;
    int e = -1;

    test_counter(save_scan_called);

    /* NOTE: here we dup the descriptor so that our copy is not closed
     * when we invoke closedir.  The iteration over directory entries will
     * however affect the offset of 'save->dirfd'.  This is OK, since
     * save_scan_feeds is protected from a second execution, but if it
     * wasn't so, the second iteration would yield no result. */
    int dirfd = dup(save->dirfd);

    if (dirfd == -1) {
        save->last_error = err_set(sfe_system_error);
        warn("dup failed");
        goto exit;
    }

    dir = fdopendir(dirfd);
    if (!dir) {
        save->last_error = err_set(sfe_system_error);
        warn("fdopendir failed");
        goto exit;
    }
    dirfd = -1;

    bool feed_addition = save->feed_addition;
    while (errno = 0, dent = readdir(dir)) {
        feed_t *feed;
        struct stat sb;

        if (fstatat(save->dirfd, dent->d_name, &sb, 0) == -1) {
            save->last_error = err_set(sfe_system_error);
            warn("fstatat(%d, %s) failed", save->dirfd, dent->d_name);
            goto exit;
        }

        if (!S_ISREG(sb.st_mode)) {
            if (dent->d_name[0] != '.')
                warnx("ignoring file %s", dent->d_name);
            continue;
        }

        if (feed_addition) {
            /* If a feed was added via save_add_feed, we need to make sure
             * it is not duplicated.  Otherwise this is already ensured by
             * the filesystem allowing only unique names. */
            feed = save_find_feed(save, dent->d_name);
            if (feed)
                continue;
        }

        feed = save_add_feed(save, dent->d_name);
        if (!feed)
            warnx("ignoring file %s", dent->d_name);
    }

    if (errno)
        goto exit;

    e = 0;

exit:
    if (dir)
        closedir(dir);
    if (dirfd != -1)
        close(dirfd);
    return e;
}

static feed_t *save_find_feed(savefile_t *save, const char *fname)
{
    feed_t *feed;

    HASH_FIND(hh, save->feeds, fname, strlen(fname), feed);
    return feed;
}

static bool save_is_bogus_fname(const char *fname)
{
    switch(fname[0]) {
        case '\0':
        case '.':
        case '\n':
        case '\r':
        case '\t':
            return true;
    }

    for (int i = 1; fname[i]; ++i)
        switch (fname[i]) {
            case '/':
            case '\n':
            case '\r':
            case '\t':
                return true;
        }

    return false;
}

static feed_t *save_add_feed(savefile_t *save, const char *fname)
{
    feed_t *feed;

    if (save_is_bogus_fname(fname)) {
        save->last_error = err_set(sfe_bad_input);
        return NULL;
    }

    feed = feed_new(save->dirfd, fname);
    if (!feed) {
        save->last_error = err_set(sfe_system_error);
        return NULL;
    }

    test_counter(added_feeds);

    /* NOTE 1: the 3rd argument of HASH_ADD is the field 'name', so
     *   we are  using &feed->name[feed_name_offset] as key.
     *
     * NOTE 2: here we assume that no entry named like 'fname' exists.
     */
    HASH_ADD(hh, save->feeds, name[feed_name_offset], strlen(fname), feed);
    save->feed_addition = true;
    return feed;
}

savefile_t * savefile_new(int basedir)
{
    savefile_t *save;

    if (fcntl(basedir, F_GETFD) == -1) {
        warn("fcntl %d failed (invalid fd?)", basedir);
        return NULL;
    }

    save = malloc(sizeof(savefile_t));
    if (!save) {
        warn("malloc failed");
        return NULL;
    }

    *save = (savefile_t){
        .dirfd = basedir,
    };

    return save;
}

savefile_err_t savefile_get_error(const savefile_t *save)
{
    return err_get(save->last_error);
}

void savefile_reset_error(savefile_t *save)
{
    save->last_error = err_set(sfe_none);
}

feed_t *savefile_get_feed(savefile_t *save, const char *fname)
{
    feed_t *feed = save_find_feed(save, fname);

    if (!feed) {
        feed = save_add_feed(save, fname);

        if (!feed)
            return NULL;
    }

    if (feed_can_be_written(feed))
        return feed;

    save->last_error = err_set(sfe_protected_file);
    return NULL;    /* note: feed is owned by savefile */
}

int savefile_load_all(savefile_t *save)
{
    int fail;

    if (save->basedir_scanned)
        return 0;

    fail = save_scan_feeds(save);
    if (!fail)
        save->basedir_scanned = true;
    return fail;
}

feed_t * savefile_iter_feeds(savefile_t *save, void **aux)
{
    feed_t *feed;

    feed = *aux;
    if (feed == NULL)
        feed = save->feeds;  /* first invocation */
    else
        feed = feed->hh.next;

    while (feed && !feed_can_be_written(feed))
        feed = feed->hh.next;

    *aux = feed;
    return feed;
}

const feed_t * savefile_citer_feeds(const savefile_t *save, void **aux)
{
    return savefile_iter_feeds((savefile_t *)save, aux);
}

bool feed_exists(const feed_t *feed)
{
    return feed->file_status != fs_noent;
}

int feed_set_provided_url(feed_t *feed, str_t *p_url)
{
    feed_set_str_field(feed, &feed->url.provided, p_url);
    return 0;
}

int feed_set_effective_url(feed_t *feed, str_t *e_url)
{
    feed_set_str_field(feed, &feed->url.effective, e_url);
    return 0;
}

int feed_set_outfmt(feed_t *feed, str_t *outfmt)
{
    feed_set_str_field(feed, &feed->outfmt, outfmt);
    return 0;
}

int feed_set_subproc_chdir(feed_t *feed, str_t *path)
{
    feed_set_str_field(feed, &feed->subproc_chdir, path);
    return 0;
}

void feed_mark_deleted(feed_t *feed)
{
    feed->marked_for_deletion = true;
}

int feed_set_items(feed_t *feed, const feed_items_t *new_items)
{
    feed_clear_items(feed);

    for (unsigned i = 0; i < new_items->n_items; ++i) {
        str_t item = {};

        if (str_dup(&item, &new_items->items[i]))
            return -1;

        if (feed_add_item(feed, &item)) {
            str_free(&item);
            return -1;
        }
    }

    feed_flag_to_write(feed);
    return 0;
}

str_t feed_get_provided_url(const feed_t *feed)
{
    return STR_ALIAS(&feed->url.provided);
}

str_t feed_get_effective_url(const feed_t *feed)
{
    return STR_ALIAS(&feed->url.effective);
}

const char *feed_get_name(const feed_t *feed)
{
    return feed_name(feed);
}

str_t feed_get_outfmt(const feed_t *feed)
{
    return STR_ALIAS(&feed->outfmt);
}

str_t feed_get_subproc_chdir(const feed_t *feed)
{
    return STR_ALIAS(&feed->subproc_chdir);
}

void feed_set_items_count(feed_t *feed, unsigned v)
{
    if (feed->items_count == v)
        return;

    feed->items_count = v;
    feed_flag_to_write(feed);
}

unsigned feed_get_items_count(const feed_t *feed)
{
    return feed->items_count;
}

feed_items_t feed_get_items(const feed_t *feed)
{
    return (feed_items_t){
        .items = feed->items,
        .n_items = feed->n_items,
    };
}

const char *feed_output_mode_to_str(feed_output_mode_t om)
{
    static const char *repr[] = {
        [feed_om_print]     = "feed_om_print",
        [feed_om_pretty]    = "feed_om_pretty",
        [feed_om_pipe]      = "feed_om_pipe",
        [feed_om_subproc]   = "feed_om_subproc",
    };
    return repr[om];
}

void feed_set_output_mode(feed_t *feed, feed_output_mode_t om)
{
    feed_set_flag(feed, fi_output_mode, om);
}

feed_output_mode_t feed_get_output_mode(const feed_t *feed)
{
    return feed->flags[fi_output_mode];
}

const char *feed_url_type_to_str(feed_url_type_t ut)
{
    static const char *repr[] = {
        [feed_ut_unknown]   = "feed_ut_unknown",
        [feed_ut_local]     = "feed_ut_local",
        [feed_ut_remote]    = "feed_ut_remote",
    };
    return repr[ut];
}

void feed_set_url_type(feed_t *feed, feed_url_type_t ut)
{
    feed_set_flag(feed, fi_url_type, ut);
}

feed_url_type_t feed_get_url_type(const feed_t *feed)
{
    return feed->flags[fi_url_type];
}

void savefile_free(savefile_t *save)
{
    feed_t *feed, *tmp;

    if (!save)
        return;

    HASH_ITER(hh, save->feeds, feed, tmp) {
        HASH_DEL(save->feeds, feed);
        feed_free(feed);
    }

    if (save->dirfd != -1)
        close(save->dirfd);

    free(save);
}
