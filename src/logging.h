/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

extern unsigned g_verbosity_level;

#define whisper_enabled (g_verbosity_level >= 2)
#define say_enabled (g_verbosity_level >= 1)

#define say(...) do { \
    if (say_enabled) \
        warnx(__VA_ARGS__); \
    } while(0)

#define whisper(...) do { \
    if (whisper_enabled) \
        warnx(__VA_ARGS__); \
    } while(0)
