/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

/* str.h - Lightweight abstraction around strings. */

#include <string.h>
#include <stdbool.h>

typedef struct {
    const char *bytes;
    unsigned len;
    bool owned;
} str_t;

#define STR_CAST(s) (str_t){ .bytes = (s), .len = (s) ? strlen(s) : 0 }
#define STR_DEFINE(s) (str_t){ .bytes = (s), .len = sizeof(s) - 1 }
#define STR_ALIAS(str) (str_t){ .bytes = (str)->bytes, .len = (str)->len }

/* Usage:
 *
 *  str_t s = STR_DEFINE("hello world");
 *  printf("string contains: %.*s\n", STR_FMT(&s));
 *
 */
#define STR_FMT(s) (int)((s)->len), (s)->bytes

/* Plain assignment of the fields of a `str_t`.
 *
 * By setting the `owned` parameter to true, the caller claim that the memory
 * pointed by `bytes` is allocated with malloc, and that the ownership of it
 * is now held by the `str` object.
 */
void str_assign(str_t *, const char *bytes, unsigned len, bool owned);

/* Akin to the C++ std::move operator.
 *
 * If `src` owns the pointed memory, the ownership is moved to `dst`,
 * otherwise it is just a simple ponter movement.  This means that `dst`
 * will gain ownership only if `src` had ownership in the first place.
 */
void str_move(str_t *dst, str_t *src);

/* Duplicate the string pointed by `src` into `dst`.
 *
 * This opeeration gives place to a memory allocation, so it might fail.
 * The return value is 0 on success and -1 on failure (errno is set).
 */
int str_dup(str_t *dst, const str_t *src);

/* Compare two strings, same semantics as strcmp/strncmp etc */
int str_cmp(const str_t *, const str_t *);

/* Compare up to the first len bytes of two strings.
 *
 * Differences beyond the boundary of 'len' bytes will be ignored.
 */
int str_cmp_up_to(const str_t *, const str_t *, unsigned len);

/* Obtain a substring of a given string.
 *
 * The returned substring won't retain any ownership.
 *
 * str_sub(s, 0, s->len) is equivalent to STR_ALIAS(s).
 */
str_t str_sub(const str_t *, unsigned start, unsigned len);

/* Copy the str_t in a null-terminated "C string".
 *
 * Returns the given buffer on success, and NULL if the string was too
 * long for the given buffer.
 */
const char *str_to_cstr(const str_t *, char *buffer, size_t len);

/* Reset the `str_t` object, call `free(3)` if the pointed memory is
 * owned by it. */
void str_free(str_t *);
