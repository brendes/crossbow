/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

/* This module provides instrumentation to embed in source modules for
 * sake of testing.  The definitions are enabled only if the TESTING
 * preprocessor symbol is defined, which only happens when compiling with
 * unit tests.  Regular binaries are not affected.
 *
 * It is important to enable "Per-Object flags emulation", as specified by
 * the Automake manual in order to avoid to accidentally enable
 * instrumentation in the distributed binaries.
 */
#ifdef TESTING

void test_reset_counters();
const unsigned test_read_counter(const char *name);

#define test_counter(name) do { \
    unsigned *test_next_counter(const char *); \
    static unsigned *cnt = NULL; \
    if (!cnt) \
        cnt = test_next_counter(#name); \
    ++(*cnt); \
} while (0)

#else   /* ifdef TESTING */

#define test_counter(...)
#define test_reset_counters(...)

#endif  /* ifdef TESTING */
