/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdlib.h>

#include "radix.h"

typedef struct node {
    struct node *next;
    struct node *sub;
    void *data;
    char symbol;
} node_t;

struct radix {
    node_t *root;
};

int radix_lookup(radix_t r,
                 const char *path,
                 int pathlen,
                 void **out)
{
    node_t *c = r->root;
    int depth = 0;
    void *found = NULL;

    if (pathlen == 0) {
        errno = EINVAL;
        return -1;
    }

    for (int i = 0; i < pathlen; ++i) {

        while (c && c->symbol < path[i])
            c = c->next;

        if (!c || c->symbol > path[i])
            goto exit;

        found = c->data;
        depth = i + 1;
        c = c->sub;
    }

exit:
    if (!found)
        return -1;
    *out = found;
    return depth;
}

int radix_register(radix_t r, const char *path, void *item)
{
    node_t *c = r->root;
    node_t **back = &r->root;

    for (int i = 0; path[i]; ++i) {

        while (c && c->symbol < path[i]) {
            back = &c->next;
            c = c->next;
        }

        if (!c || c->symbol > path[i]) {
            node_t *n = malloc(sizeof(node_t));
            if (!n)
                return -1;

            *n = (node_t){
                .symbol = path[i],
            };

            *back = n;
            n->next = c;
            c = n;
        }

        if (path[i + 1] == '\0') {
            c->data = item;
            return 0;
        }

        back = &c->sub;
        c = c->sub;
    }

    errno = EINVAL;
    return -1;
}

static void node_free(node_t *n)
{
    if (!n)
        return;

    node_free(n->next);
    node_free(n->sub);
    free(n);
}

radix_t radix_new(void)
{
    radix_t r;

    r = malloc(sizeof(struct radix));
    if (r)
        *r = (struct radix){};
    return r;
}

void radix_free(radix_t r)
{
    if (!r)
        return;

    node_free(r->root);
    free(r);
}
