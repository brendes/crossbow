/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "text.h"

#include <ctype.h>
#include <stdint.h>

enum
{
    dots_len = 3,
};

const char *end_with_dots(char *dst, size_t dst_size)
{
    switch (dst_size) {
        default:    dst[dst_size - 4] = '.';
        case 3:     dst[dst_size - 3] = '.';
        case 2:     dst[dst_size - 2] = '.';
        case 1:     dst[dst_size - 1] = '\0';
        case 0:     return dst;
    }
}

const char *text_short(char *dst, size_t dst_size,
                       const char *text,
                       const size_t *opt_text_len)
{
    size_t j = 0;
    size_t max_len = opt_text_len ? *opt_text_len : SIZE_MAX;
    enum {
        normal,
        skip_empty
    } phase = normal;

    #define APPEND(c) do {                          \
        if (j < dst_size - 1)                       \
            dst[j++] = c;                           \
        else                                        \
            return end_with_dots(dst, dst_size);    \
    } while (0)

    if (!text || dst_size == 0)
        return NULL;

    for (size_t i = 0; i < max_len && text[i]; ++i) {
        char c = text[i];

        switch (phase) {
            case normal:
                if (c == '\n') {
                    APPEND(c);
                    APPEND(' ');
                    APPEND(' ');
                    phase = skip_empty;
                    continue;
                }

                if (!isprint(c)) {
                    APPEND('.');
                    continue;
                }
                break;

            case skip_empty:
                if (isspace(c))
                    continue;
                phase = normal;
        }

        APPEND(c);
    }
    #undef APPEND

    dst[j] = 0;
    return dst;
}

const char *text_join_argv(char *dst, size_t dst_size,
                           const char * const *argv)
{
    size_t j = 0;

    #define APPEND(c) do {                          \
        if (j < dst_size - 1)                       \
            dst[j++] = c;                           \
        else                                        \
            return end_with_dots(dst, dst_size);    \
    } while (0)

    for (size_t i = 0; argv[i]; ++i) {
        if (i)
            APPEND(' ');

        for (size_t k = 0; argv[i][k]; ++k) {
            char c = argv[i][k];
            switch (c) {
                case ' ':
                case '\\':
                    APPEND('\\');
                default:
                    break;
            }
            APPEND(c);
        }
    }
    #undef APPEND

    dst[j] = 0;
    return dst;
}
