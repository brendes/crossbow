/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sysexits.h>
#include <unistd.h>

#include "config.h"
#include "help_helper.h"
#include "logging.h"
#include "outfmt.h"
#include "placeholders.h"
#include "subcmd_common.h"
#include "savefile_aux.h"
#include "url_helper.h"

struct opts
{
    const char *uid;
    const char *url;
    const char *outfmt;
    const char *subproc_chdir;
    feed_output_mode_t output_mode;
    feed_url_type_t url_type;
    bool output_mode_set : 1;
    bool url_type_set : 1;
};

typedef struct opts opts_t;

static const help_flag_t help_flags[] = {
    {
        .optname = "-C",
        .argname = "DIR",
        .description = "sub-processes chdir to DIR before invoking exec"
    }, {
        .optname = "-f",
        .argname = "FORMAT",
        .description = "output format",
    }, {
        .optname = "-i",
        .argname = "ID",
        .description = "feed identifier",
    }, {
        .optname = "-o",
        .argname = "MODE",
        .description = "output mode (print, pretty, subproc, pipe)",
    }, {
        .optname = "-t",
        .argname = "TYPE",
        .description = "claim URL type (local, remote)",
    }, {
        .optname = "-u",
        .argname = "URL",
        .description = "URL to fetch for updates",
    }, {}
};

static const struct help help = {
    .progname = "crossbow-set",
    .flags = help_flags,
};

static inline feed_output_mode_t parse_output_mode(const char *optarg)
{
    static const struct enum_keyval options[] = {
        {"print",   feed_om_print},
        {"pretty",  feed_om_pretty},
        {"subproc", feed_om_subproc},
        {"pipe",    feed_om_pipe},
        {}
    };

    return parse_enum('o', options, optarg);
}

static inline feed_url_type_t parse_url_type(const char *optarg)
{
    static const struct enum_keyval options[] = {
        {"unknown", feed_ut_unknown},
        {"local",   feed_ut_local},
        {"remote",  feed_ut_remote},
        {}
    };

    return parse_enum('u', options, optarg);
}

static opts_t read_opts(int argc, char **argv)
{
    int opt;

    opts_t result = {};

    if (optind >= argc)
        show_help(help_usage, &help, "missing mandatory identifier");

    if (argv[optind][0] != '-')
        /* it is not an option, so it must be the uid.  Should the user want a
         * uid which is actually starting with a hypen, it can be supplied by
         * means of the -i flag. */
        result.uid = argv[optind++];

    while (opt = getopt(argc, argv, "C:f:i:o:t:u:vh"), opt != -1) {
        switch (opt) {
        case 'C':
            result.subproc_chdir = optarg;
            break;

        case 'f':
            result.outfmt = optarg;
            break;

        case 'i':
            result.uid = optarg;
            break;

        case 'o':
            result.output_mode = parse_output_mode(optarg);
            result.output_mode_set = true;
            break;

        case 't':
            result.url_type = parse_url_type(optarg);
            result.url_type_set = true;
            break;

        case 'u':
            result.url = optarg;
            break;

        case 'h':
            show_help(help_full, &help, NULL);
            break;

        case 'v':
            g_verbosity_level++;
            break;

        default:
            show_help(help_usage, &help, NULL);
        }
    }

    if (!result.uid)
        show_help(help_usage, &help, "missing mandatory identifier");

    if (!result.url && optind < argc)
        result.url = argv[optind++];

    for (int i = optind; i < argc; ++i)
        warnx("ignoring argument: %s", argv[i]);

    return result;
}

static int check_outfmt(const char *outfmt, feed_output_mode_t output_mode)
{
    int e = -1;
    ofmt_t ofmt = NULL;
    ofmt_mode_t ofmt_mode;

    if (convert_out_mode(output_mode, &ofmt_mode) == -1) {
        warnx("output format will be ignored: output mode is %s",
              feed_output_mode_to_str(output_mode));
        return 0;
    }

    ofmt = ofmt_new();
    if (!ofmt) {
        warn("ofmt_new");
        goto exit;
    }

    if (placeholders_setup(ofmt) == -1)
        goto exit;

    whisper("trying to compile: %s", outfmt);
    if (ofmt_compile(ofmt, ofmt_mode, outfmt, strlen(outfmt)))
        ofmt_print_error(ofmt);
    else
        e = 0;

exit:
    if (e)
        warnx("unable to parse output specification \"%s\"", outfmt);
    ofmt_del(ofmt);
    return e;
}

static void warn_unless_exists(const str_t *path_str)
{
    char path[PATH_MAX];
    struct stat statbuf;

    if (str_to_cstr(path_str, path, sizeof(path)) == NULL) {
        warnx("horribly long path name: %.*s", STR_FMT(path_str));
        return;
    }

    if (stat(path, &statbuf) == 0)
        return;

    warn("warning, stat(%s) failed", path);
}

static int set_url(feed_t *feed, const opts_t *opts)
{
    if (!opts->url) {
        if (!feed_exists(feed)) {
            /* The savefile does not exist.  This means it is the first
             * time we create the feed.  The URL *must* be provided. */
            warn("a URL is required for the initial feed setup");
            return -1;
        }

        if (!opts->url_type_set)
            return 0;   /* Nothing is set, feed exists, nothing needs to
                         * be changed */
    }

    /* Each feed of a sendfile has two URL fields: the 'provided URL' and
     * the 'effective URL'.  The 'provided URL' stores the verbatim URL
     * provided by the user.  The 'effective URL' stores the URL where
     * the ATOM/RSS XML gets fetched.
     *
     * In most situation the two URLs are the same.  The two differ if,
     * for example, the user's 'provided URL' is just
     * "example.com/feed.rss", which is translated into the 'effective
     * URL' "https://example.com/feed.rss".
     *
     * If the opts_t structure does not provide anything, we use whatever
     * is stored in the savefile.  The conditional in the beginning of
     * set_url guarantees us to end up with some legal value.
     */
    url_pair_t provided = {
        .type = opts->url_type_set
              ? opts->url_type
              : feed_get_url_type(feed),
        .url = opts->url
             ? STR_CAST(opts->url)
             : feed_get_provided_url(feed),
    };
    url_pair_t effective = {};
    int e = -1;

    if (url_get_effective(&provided, &effective)) {
        warnx("url_get_effective failed");
        return -1;
    }

    whisper("set effective url: location '%.*s' type %s",
        STR_FMT(&effective.url),
        feed_url_type_to_str(effective.type)
    );

    if (effective.type == feed_ut_local)
        warn_unless_exists(&effective.url);

    if (feed_set_provided_url(feed, &provided.url)) {
        warnx("feed_set_provided_url failed");
        goto exit;
    }

    if (feed_set_effective_url(feed, &effective.url)) {
        warnx("feed_set_provided_url failed");
        goto exit;
    }

    feed_set_url_type(feed, effective.type);
    e = 0;
exit:
    str_free(&effective.url);
    return e;
}

static int set_subproc_chdir(feed_t *feed, const opts_t *opts)
{
    if (!opts->subproc_chdir)
        return 0;

    char path[PATH_MAX];

    if (realpath(opts->subproc_chdir, path) == NULL) {
        warn("realpath(%s) failed", opts->subproc_chdir);
        return -1;
    }

    whisper("set subproc chdir: %s", path);
    return feed_set_subproc_chdir(
        feed,
        &(str_t){
            .bytes = path,
            .len = strlen(path),
        }
    );
}

static int setup_feed(const opts_t *opts, savefile_t *savefile)
{
    say("Feed [%s] (%s)",
        opts->uid,
        opts->url ? "creating or updating" : "expected to exist"
    );

    feed_t *feed = savefile_get_feed(savefile, opts->uid);
    if (!feed) {
        savefile_err_t e = savefile_get_error(savefile);

        switch (e.error) {
            case sfe_protected_file:
                warnx("name \"%s\" cannot be used: incompatible file exists",
                      opts->uid);
                break;
            case sfe_bad_input:
                warnx("name \"%s\" is probably bogus", opts->uid);
                break;
            case sfe_system_error:
                errno = e.errno_val;
                warn("savefile_get_feed failed");
                break;
            default:
                abort();
        }
        return -1;
    }

    if (!feed_exists(feed) && !opts->url) {
        warnx("a URL is required for the initial feed setup");
        return -1;
    }

    if (set_url(feed, opts)) {
        warnx("failed to set url");
        return -1;
    }

    feed_output_mode_t output_mode;
    if (opts->output_mode_set) {
        whisper(
            "set output mode: %s",
            feed_output_mode_to_str(opts->output_mode)
        );
        feed_set_output_mode(feed, opts->output_mode);
        output_mode = opts->output_mode;
    }
    else
        output_mode = feed_get_output_mode(feed);

    if (opts->outfmt) {
        if (check_outfmt(opts->outfmt, output_mode) != 0)
            return -1;

        whisper("set output format: %s", opts->outfmt);
        if (feed_set_outfmt(feed, &STR_CAST(opts->outfmt)) == -1) {
            warnx("feed_set_outfmt failed");
            return -1;
        }
    }

    if (set_subproc_chdir(feed, opts)) {
        warnx("faled to set subproc chdir");
        return -1;
    }

    return feed_persist(feed);
}

int main(int argc, char **argv)
{
    opts_t opts;
    savefile_t *savefile;

#ifdef HAVE_PLEDGE
    if (pledge("stdio rpath wpath cpath",  NULL) == -1)
        err(1, "pledge");
#endif

    opts = read_opts(argc, argv);

    savefile = open_savefile(true);
    if (!savefile)
        err(EXIT_FAILURE, "savefile_load");

    int fail = setup_feed(&opts, savefile);
    savefile_free(savefile);
    return fail ? EXIT_FAILURE : EXIT_SUCCESS;
}
