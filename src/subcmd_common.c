/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sysexits.h>
#include <unistd.h>

#include "subcmd_common.h"
#include "savefile.h"

int parse_enum(char optname,
               const struct enum_keyval keyval[],
               const char *optarg)
{
    for (int i = 0; keyval[i].atom; ++i)
        if (strcmp(keyval[i].atom, optarg) == 0)
            return keyval[i].enum_value;

    warnx("invalid value \"%s\" for option -%c",
          optarg, optname);
    warnx("accepted values:");
    for (int i = 0; keyval[i].atom; ++i)
        warnx(" %s", keyval[i].atom);

    exit(EX_USAGE);
}

int convert_out_mode(feed_output_mode_t fom, ofmt_mode_t *om)
{
    /* feed_output_mode_t is the enumerative used in savefile:  it covers
     * all output modes.  ofmt_mode_t is the enumerative covered by the
     * output format module: it supports only those modes that require an
     * output format.  There's a semantic overlapping. */

    if (fom == feed_om_print)
        return -1;

    static const ofmt_mode_t map[] = {
        [feed_om_pretty]    = ofmt_mode_print,
        [feed_om_pipe]      = ofmt_mode_pipe,
        [feed_om_subproc]   = ofmt_mode_subprocess,
    };

    *om = map[fom];
    return 0;
}
