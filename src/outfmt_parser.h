/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <stddef.h>

typedef enum {
    ofp_atom_pholder,
    ofp_atom_verbatim,
    ofp_atom_whitespace,
} ofp_atom_t;

typedef struct {
    const char *begin;
    size_t len;
    ofp_atom_t atom;
} ofp_token_t;

const char * ofp_atom_str(ofp_atom_t);

typedef enum {
    ofp_fail_none = 0,

    ofp_fail_bad_spec,
    ofp_fail_bad_state,
    ofp_fail_emitting,
    ofp_fail_invalid_pholder,
    ofp_fail_trail_escape,
} ofp_fail_t;

const char * ofp_fail_str(ofp_fail_t);

struct ofp_setup {
    void *opaque;
    int (*emit)(void *, const ofp_token_t *);
    void (*fail)(void *, ofp_fail_t);
};

typedef const struct ofp_setup * ofp_setup_t;

int ofp_scan(ofp_setup_t, const char *spec, size_t speclen);
