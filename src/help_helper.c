/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "help_helper.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

struct help_context {
    size_t max_optname_len;
    size_t max_argname_len;
};

static inline void show_help_line(const struct help_context *ctx,
                                  const help_flag_t *help_flag)
{
    fprintf(stderr, "  %*s %-*s %s\n",
        (int)ctx->max_optname_len,
        help_flag->optname ?: "",
        (int)ctx->max_argname_len,
        help_flag->argname ?: "",
        help_flag->description ?: ""
    );
}

static void show_usage(const struct help *help)
{
    fputs("usage: ", stderr);
    fputs(help->progname, stderr);
    fputs(" [-h] [-v]", stderr);

    if (help->flags)
        for (int i = 0; help->flags[i].optname; ++i) {
            fputs(" [", stderr);
            fputs(help->flags[i].optname, stderr);
            if (help->flags[i].argname) {
                fputc(' ', stderr);
                fputs(help->flags[i].argname, stderr);
            }
            fputc(']', stderr);
        }

    if (help->args) {
        fputc(' ', stderr);
        fputs(help->args, stderr);
    }
    fputc('\n', stderr);
}

void show_help(help_type_t type, const struct help *help, const char *warning)
{
    if (warning)
        fprintf(stderr, "%s\n", warning);

    show_usage(help);
    if (type == help_usage)
        exit(EX_USAGE);

    struct help_context ctx = {
        .max_optname_len = 2,   /* strlen("-h") == strlen("-v") == 2 */
        .max_argname_len = 10,  /* some space by default */
    };

    if (help->flags) {
        for (int i = 0; help->flags[i].optname; ++i) {
            size_t len;

            len = strlen(help->flags[i].optname);
            if (len > ctx.max_optname_len)
                ctx.max_optname_len = len;

            if (!help->flags[i].argname)
                continue;

            len = strlen(help->flags[i].argname);
            if (len > ctx.max_argname_len)
                ctx.max_argname_len = len;
        }

        fputs("Options:\n", stderr);
        for (int i = 0; help->flags[i].optname; ++i) {
            show_help_line(&ctx, &help->flags[i]);
        }
    }

    show_help_line(&ctx, &(const help_flag_t){
        .optname = "-h",
        .description = "print this help",
    });
    show_help_line(&ctx, &(const help_flag_t){
        .optname = "-v",
        .description = "increase verbosity",
    });

    exit(type == help_usage ? EX_USAGE : EXIT_SUCCESS);
}
