#include "savefile_aux.h"

#include "logging.h"

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

static int open_dir(const char *path, bool create)
{
    int dfd;

    dfd = open(path, O_DIRECTORY | O_CLOEXEC);

    if (dfd != -1)
        return dfd;

    if (errno != ENOENT)
        warn("unable to open %s (as directory)", path);
    else if (create) {
        mkdir(path, 0755);
        dfd = open(path, O_DIRECTORY | O_CLOEXEC);
    }

    if (dfd == -1)
        warn("opening directory %s (%d)", path, errno);

    return dfd;
}

static int vardir(bool create)
{
    char buffer[PATH_MAX];
    const char *path;

    path = getenv("CROSSBOW_VAR");
    if (path)
        goto chosen;

    path = getenv("HOME");
    if (path) {
        int n = snprintf(buffer, sizeof(buffer), "%s/.crossbow", path);
        if (n >= sizeof(buffer)) {
            warnx("truncation in filename: %s/%s (buffer size: %zu)",
                  buffer, path, sizeof(buffer));
            return -1;
        }

        path = buffer;
        goto chosen;
    }

    path = "/var/lib/crossbow";

chosen:
    whisper("configuration in \"%s\"", path);
    return open_dir(path, create);
}

savefile_t *open_savefile(bool create)
{
    int vdfd;
    savefile_t *savefile;

    vdfd = vardir(create);
    if (vdfd == -1)
        return NULL;

    savefile = savefile_new(vdfd);
    if (!savefile) {
        int errno_s = errno;;
        if (close(vdfd) == -1)
            warn("close(%d)", vdfd);
        errno = errno_s;
    }

    return savefile;
}

