/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "test_counters.h"

#include <err.h>
#include <string.h>
#include <sysexits.h>

enum
{
    test_max_counters = 30,
};

struct test_counter
{
    const char *name;
    unsigned value;
};

typedef struct test_counter test_counter_t;

static test_counter_t test_counters[test_max_counters];
static unsigned next_counter;

unsigned *test_next_counter(const char *name)
{
    test_counter_t *out;

    if (next_counter >= test_max_counters)
        errx(1, "too many test counters");

    out = &test_counters[next_counter++];
    out->name = name;

    return &out->value; /* already zero, because it's in the BSS */
}

const unsigned test_read_counter(const char *name)
{
    for (int i = 0; i < next_counter; ++i)
        if (strcmp(test_counters[i].name, name) == 0)
            return test_counters[i].value;

    return 0;
}

void test_reset_counters(void)
{
    for (int i = 0; i < next_counter; ++i)
        test_counters[i].value = 0;
}
