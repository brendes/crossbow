/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#include "launcher.h"
#include "outfmt_parser.h"
#include "radix.h"

typedef struct ofmt * ofmt_t;

ofmt_t ofmt_new(void);

void ofmt_set_opaque(ofmt_t, void *opaque, void (*opaque_free)(void *));
void *ofmt_get_opaque(ofmt_t);

typedef const char * (*ofmt_resolver_t)(void *item, void *opaque);
int ofmt_set_resolver(ofmt_t, const char *key, ofmt_resolver_t);
int ofmt_set_pipe_resolver(ofmt_t, ofmt_resolver_t);

typedef enum {
    ofmt_mode_pipe,
    ofmt_mode_print,
    ofmt_mode_subprocess,
} ofmt_mode_t;

int ofmt_compile(ofmt_t, ofmt_mode_t, const char *spec, size_t speclen);

typedef struct {
    void *item;
    const int *opt_print_fd;
    const char *opt_subproc_chdir;
    bool dry_run;
} ofmt_evaluate_params_t;

int ofmt_evaluate(ofmt_t, const ofmt_evaluate_params_t *);

typedef enum {
    ofmt_fail_none = 0,

    ofmt_fail_bad_setup,
    ofmt_fail_bad_state,
    ofmt_fail_invalid_pholder,
    ofmt_fail_parse,
    ofmt_fail_subproc,
    ofmt_fail_system,
    ofmt_fail_too_long_evaluation,
    ofmt_fail_too_long_spec,
    ofmt_fail_too_many_args,
    ofmt_fail_too_many_pholders,
    ofmt_fail_undefined_resolver,
} ofmt_fail_t;

const char * ofmt_fail_str(ofmt_fail_t);

typedef struct {
    ofmt_fail_t reason;
    union {
        ofp_fail_t ofp_reason;  /* when reason == ofmt_fail_parse */
        int errno_val;          /* when reason == ofmt_fail_system */
        struct {                /* when reason == ofmt_fail_subproc */
            const char *argv0;
            int exit_status;
        } subproc;
    };
} ofmt_err_t;

const ofmt_err_t * ofmt_get_error(ofmt_t ofmt);

void ofmt_print_error(ofmt_t ofmt);

void ofmt_del(ofmt_t);
