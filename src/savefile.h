/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdbool.h>
#include "str.h"

enum
{
    feed_max_items = 1024,
};

typedef struct savefile_t savefile_t;
typedef struct feed_t feed_t;

savefile_t * savefile_new(int basedir);

typedef struct {
    enum {
        sfe_none = 0,
        sfe_system_error,   /* system failure, check errno for details */
        sfe_protected_file, /* unrecognized or incompatible */
        sfe_bad_input,      /* invalid input, e.g. feed name */
    } error;
    int errno_val;          /* valid if error == sfe_system_error */
} savefile_err_t;

/* Return the last error recorded by the savefile */
savefile_err_t savefile_get_error(const savefile_t *);

/* reset internal error */
void savefile_reset_error(savefile_t *);

feed_t * savefile_get_feed(savefile_t *, const char *name);

/* Load all feeds from the base directory.
 *
 * The directory is not scanned by default in savefile_new, since loading
 * all the feeds is not always needed.
 *
 * Calling this function is a necessary step before the invocation of
 * savefile_iter_feeds or savefile_citer_feeds.  The iteration over the
 * items of a feed via feed_citer_items is not affected.
 */
int savefile_load_all(savefile_t *);

/* Iterator over feeds.
 *
 * The 'aux' parameter is an auxiliary pointer used internally to track
 * the advancement of the iteration.  It must be set to NULL before the
 * iteration.
 *
 * The iteration is over when the returned pointer is NULL
 */
feed_t * savefile_iter_feeds(savefile_t *, void **aux);

/* Constant iteration over feeds.
 *
 * Works in the same way as savefile_iter_feeds
 */
const feed_t * savefile_citer_feeds(const savefile_t *, void **aux);

void savefile_free(savefile_t *);

bool feed_exists(const feed_t *);

typedef struct {
    str_t const * const items;
    unsigned n_items;
} feed_items_t;

feed_items_t feed_get_items(const feed_t *feed);
int feed_set_items(feed_t *, const feed_items_t *);

/* setters, they use move semantics on strings */
int feed_set_provided_url(feed_t *, str_t *p_url);
int feed_set_effective_url(feed_t *, str_t *e_url);
int feed_set_outfmt(feed_t *, str_t *outfmt);
int feed_set_subproc_chdir(feed_t *, str_t *path);

void feed_mark_deleted(feed_t *);

const char *feed_get_name(const feed_t *);

/* getters, returned strings never have ownership */
str_t feed_get_provided_url(const feed_t *);
str_t feed_get_effective_url(const feed_t *);
str_t feed_get_outfmt(const feed_t *);
str_t feed_get_subproc_chdir(const feed_t *);

void feed_set_items_count(feed_t *, unsigned);
unsigned feed_get_items_count(const feed_t *);

typedef enum feed_output_mode {
    feed_om_print,
    feed_om_pretty,
    feed_om_pipe,
    feed_om_subproc,
} feed_output_mode_t;

const char *feed_output_mode_to_str(feed_output_mode_t);
void feed_set_output_mode(feed_t *, feed_output_mode_t);
feed_output_mode_t feed_get_output_mode(const feed_t *);

typedef enum feed_url_type {
    feed_ut_unknown,
    feed_ut_local,
    feed_ut_remote,
} feed_url_type_t;

const char *feed_url_type_to_str(feed_url_type_t);
void feed_set_url_type(feed_t *, feed_url_type_t);
feed_url_type_t feed_get_url_type(const feed_t *);

int feed_persist(feed_t *);
