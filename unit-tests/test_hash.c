/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "hash.h"
#include "str.h"

#include "util.h"
#include "unittest.h"

static int set_semantics(util_ctx_t ctx, intptr_t opaque)
{
    hash_t iset;

    iset = HASH_INIT;
    EXPECT(hash_insert(&iset, &STR_DEFINE("hello"), NULL) == 0);
    EXPECT(hash_contains(&iset, &STR_DEFINE("hello")));

    EXPECT(hash_insert(&iset, &STR_DEFINE("world"), NULL) == 0);
    EXPECT(hash_contains(&iset, &STR_DEFINE("world")));

    /* Empty str allowed */
    EXPECT(hash_insert(&iset, &(str_t){}, NULL) == 0);
    EXPECT(hash_contains(&iset, &(str_t){}));

    void *aux = NULL;
    hash_pair_t pair;
    struct {
        short null, hello, world;
    } seen = {};

    while (pair = hash_iter(&iset, &aux), pair.key)
        if (!pair.key->bytes)
            seen.null++;
        else if (strcmp(pair.key->bytes, "hello") == 0)
            seen.hello++;
        else if (strcmp(pair.key->bytes, "world") == 0)
            seen.world++;
        else
            FAIL(pair.key->bytes);

    hash_free(&iset);
    EXPECT(seen.null == 1);
    EXPECT(seen.hello == 1);
    EXPECT(seen.world == 1);

    return 0;
}

const struct test * list_test(void)
{
    static struct test tests[] = {
        TEST(1, set_semantics, NULL),
        END_TESTS
    };
    return tests;
}
