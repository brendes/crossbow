/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#ifdef NDEBUG
#undef NDEBUG
#include <assert.h>
#define NDEBUG
#else
#include <assert.h>
#endif

#include "util.h"

#define FAIL(reason) \
    do { \
        fprintf(stderr, __FILE__ ":%d: %s errno=%d (%s)\n", \
                __LINE__, \
                (reason), \
                errno, strerror(errno) \
        ); \
        return -1; \
    } while(0)

#define EXPECT_OR(condition, otherwise) \
    do { \
        if (!(condition)) { \
            fprintf(stderr, __FILE__ ":%d: error: ![%.40s%s] errno=(%s)\n", \
                __LINE__, \
                #condition, \
                sizeof(#condition) > 40 ? "..." : "", \
                strerror(errno) \
            ); \
            {otherwise;} \
            return -1; \
        } \
    } while(0)

#define EXPECT(condition) EXPECT_OR(condition, (void)1)

typedef int (*test_cb_t)(util_ctx_t, intptr_t opaque);

struct test {
    int enabled;
    const char *name;
    test_cb_t callback;
    intptr_t opaque;
    const char *opaque_repr;
};

const struct test * list_test();

#define TEST(enable, callback, opaque) {(enable), #callback, (callback), (intptr_t)(opaque), #opaque}
#define END_TESTS {0, NULL, NULL, 0}
