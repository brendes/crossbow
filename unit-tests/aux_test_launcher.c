/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>

typedef enum {
    mode_none,
    mode_count,
    mode_sleep,
} aux_mode_t;

static const char * const aux_mode_str[] = {
    "mode_none",
    "mode_count",
    "mode_sleep",
};

static void usage(const char *prgname, int exs)
{
    fprintf(stderr, "Usage: %s ...\n", prgname);
    exit(exs);
}

static long as_long(const char *str)
{
    long long int val;
    char *end;

    errno = 0;
    val = strtoll(str, &end, 10);

    if (end != str + strlen(str))
        errno = EINVAL;
    else if (val < LONG_MIN || val > LONG_MAX)
        errno = ERANGE;

    if (errno)
        return -1;
    return (long)val;
}

static int run_count(const char *arg)
{
    short count[256];
    int c, sum;

    for (int i = 0; i < 256; ++i)
        count[i] = 0;

    while (c = fgetc(stdin), c != EOF)
        ++count[c & 0xff];

    sum = 0;
    for (int i = 0; arg[i]; ++i)
        sum += count[(unsigned char)arg[i]];

    if (sum - (sum & 0377) > 0)
        warnx("sum is %d, but exit value will be %d",
              sum, sum & 0377);

    return sum;
}

static int run_sleep(const char *arg)
{
    long delay_ns;
    struct timespec remains, delay;

    delay_ns = as_long(arg);
    if (delay_ns == -1 && errno)
        err(EXIT_FAILURE, "invalid nanoseconds interval: %s", arg);

    delay.tv_sec = delay_ns / 1000000000;
    delay.tv_nsec = delay_ns % 1000000000;

    if (nanosleep(&delay, &remains) == -1)
        err(EXIT_FAILURE, "faulty sleep, {.tv_sec=%ld, .tv_nsec=%ld} remains",
            (long)remains.tv_sec, remains.tv_nsec);

    return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
    int opt, verbose = 0;
    aux_mode_t mode = mode_none;
    const char *mode_arg = NULL;

    while (errno = 0, opt = getopt(argc, argv, "c:hs:v"), opt != -1) {
        switch (opt) {
        case 'c':
            mode = mode_count;
            mode_arg = optarg;
            break;

        case 'h':
            usage(argv[0], EXIT_SUCCESS);

        case 's':
            mode = mode_sleep;
            mode_arg = optarg;
            break;

        case 'v':
            verbose++;
            break;

        default:
            warnx("unknown option: %c", opt);
            usage(argv[0], EX_USAGE);
        }

        if (errno) {
            warn("failed to grasp option -%c", opt);
            usage(argv[0], EX_DATAERR);
        }
    }

    if (verbose)
        warnx("executing %s", aux_mode_str[mode]);

    switch (mode) {
    case mode_count:
        exit(run_count(mode_arg));

    case mode_sleep:
        exit(run_sleep(mode_arg));

    case mode_none:
        warnx("no mode selected");
        break;
    }

    exit(EXIT_SUCCESS);
}
