/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "util.h"

#include "test_counters.h"

#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#define TEMPDIR_SUFFIX "-XXXXXX"

struct util_ctx {
    struct {
        char *path;
        int fd;
    } tempdir;

    const char *test_name;
};

#define EMPTY (struct util_ctx){ \
    .tempdir = { \
        .path = NULL, \
        .fd = -1 \
    }, \
    .test_name = NULL, \
}

static util_ctx_t util_ctx_new(void)
{
    util_ctx_t ctx;

    ctx = malloc(sizeof(*ctx));
    if (!ctx)
        return NULL;

    *ctx = EMPTY;
    return ctx;
}

static void cleardir(int dirfd)
{
    DIR *dir;
    struct dirent *dirent;

    dir = fdopendir(dirfd);
    if (!dir) {
        warn("fdopendir %d", dirfd);
        return;
    }

    while (errno = 0, dirent = readdir(dir), dirent != NULL) {
        int fd, unlinkat_flags = 0;

        if (strcmp(dirent->d_name, ".") == 0) continue;
        if (strcmp(dirent->d_name, "..") == 0) continue;

        fd = openat(dirfd, dirent->d_name, O_DIRECTORY);
        if (fd != -1) {
            cleardir(fd);
            unlinkat_flags = AT_REMOVEDIR;
        } else if (errno != ENOTDIR) {
            warn("openat %d %s (skip)", dirfd, dirent->d_name);
            continue;
        }

        if (unlinkat(dirfd, dirent->d_name, unlinkat_flags) == -1)
            warn("unlink %d %s %d (skip)", dirfd, dirent->d_name, unlinkat_flags);
    }

    if (errno)
        warn("readdir");

    closedir(dir);
}

static void rmtempdir(util_ctx_t ctx)
{
    if (ctx->tempdir.fd != -1) {
        cleardir(ctx->tempdir.fd);
        ctx->tempdir.fd = -1;
    }

    if (ctx->tempdir.path) {
        if (rmdir(ctx->tempdir.path) == -1)
            warn("rmdir %s", ctx->tempdir.path);
        free(ctx->tempdir.path);
        ctx->tempdir.path = NULL;
    }
}

static int mktempdir(util_ctx_t ctx)
{
    char *dirname;
    size_t n;
    int dirfd;

    if (ctx->tempdir.fd != -1)
        return 0;

    n = strlen(ctx->test_name) + sizeof(TEMPDIR_SUFFIX);
    dirname = malloc(n);
    if (!dirname) {
        warn("malloc");
        goto fail;
    }

    if (snprintf(dirname, n, "%s" TEMPDIR_SUFFIX, ctx->test_name) == -1) {
        warn("asprintf");
        goto fail;
    }

    if (mkdtemp(dirname) == NULL) {
        warn("mkdtemp %s", dirname);
        goto fail;
    }

    dirfd = open(dirname, O_DIRECTORY);
    if (dirfd == -1) {
        warn("open %s, O_DIRECTORY", dirname);
        goto fail;
    }

    ctx->tempdir.path = dirname;
    ctx->tempdir.fd = dirfd;
    return 0;

fail:
    free(dirname);
    return -1;
}

static void util_ctx_del(util_ctx_t ctx)
{
    if (ctx == NULL)
        return;

    rmtempdir(ctx);
    free(ctx);
}

util_ctx_t util_test_setup(const char *test_name)
{
    util_ctx_t ctx;

    ctx = util_ctx_new();
    if (!ctx)
        return NULL;

    ctx->test_name = test_name;
    return ctx;
}

void util_test_teardown(util_ctx_t ctx)
{
    util_ctx_del(ctx);
    test_reset_counters();
}

int util_open(util_ctx_t ctx, const char *path, int flags, mode_t mode)
{
    int fd;

    if (mktempdir(ctx) == -1)
        return -1;

    fd = openat(ctx->tempdir.fd, path, flags, mode);
    if (fd == -1)
        warn("openat(%d, %s, %d, %d)", ctx->tempdir.fd, path, flags, mode);
    return fd;
}

int util_mkdir(util_ctx_t ctx, const char *path, mode_t mode)
{
    if (mktempdir(ctx) == -1)
        return -1;

    return mkdirat(ctx->tempdir.fd, path, mode);
}

#define iov_set_static(I, Text) \
    do { \
        struct iovec *_I = (I); \
        _I->iov_base = (void *)(Text); \
        _I->iov_len = sizeof(Text) - 1; \
    } while (0);

static void iov_set_str(struct iovec *iov, const char *str)
{
    iov->iov_base = (void *)str;
    iov->iov_len = strlen(str);
}

static int more(const struct util_rss_item *itemiter)
{
    if (!itemiter)
        return 0;
    return itemiter->guid || itemiter->link;
}

int util_gen_rss(util_ctx_t ctx, const char *path, const struct util_rss_item *itemiter)
{
    enum { increments = 5 };

    int fd, result, errno_copy;
    int iovec_idx = 0;
    struct iovec iovec[2 * 4 * increments];

    fd = util_open(ctx, path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if (fd == -1)
        return -1;

    iov_set_static(&iovec[iovec_idx++],
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n"
        "  <channel>\n"
        "  <title>Feed Title</title>\n"
        "  <link>http://example.org</link>\n"
        "  <generator>util.c</generator>\n"
        "  <description>yada yada</description>\n"
        "  <atom:link href=\"http://example.org/feed.rss\" "
                "rel=\"self\" type=\"application/rss+xml\"/>\n"
        "  <lastBuildDate>Wed, 09 Oct 2019 00:00:00</lastBuildDate>\n"
    );

    while (more(itemiter)) {
        const int max = iovec_idx + increments;

        if (max >= sizeof(iovec) / sizeof(iovec[0]))
            break;

        iov_set_static(&iovec[iovec_idx++],
            "    <item>\n"
            "    <title>capra</title>\n"
            "    <pubDate>Wed, 09 Oct 2019 00:00:00</pubDate>\n"
            "    <description>&lt;p&gt;one two three&lt;/p&gt;</description></item>\n"
            "    <guid isPermaLink=\"false\">"
        );
        iov_set_str(&iovec[iovec_idx++], itemiter->guid);
        iov_set_static(&iovec[iovec_idx++],
            "</guid>\n"
            "    <link>"
        );
        iov_set_str(&iovec[iovec_idx++], itemiter->link);
        iov_set_static(&iovec[iovec_idx++],
            "</link>\n"
        );

        ++itemiter;
    }

    if (more(itemiter)) {
        warnx("not last: %s %s", itemiter->guid, itemiter->link);
        errx(2, "Static test iovec is too small! Set a higher value");
    }

    iov_set_static(&iovec[iovec_idx++], "</channel></rss>");

    result = writev(fd, iovec, iovec_idx);
    errno_copy = errno;
    close(fd);
    errno = errno_copy;

    return result >= 0 ? 0 : -1;
}

int util_get_tempdir(util_ctx_t ctx)
{
    if (mktempdir(ctx) == -1)
        return -1;

    return openat(ctx->tempdir.fd, ".", O_DIRECTORY);
}
