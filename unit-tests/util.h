/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <sys/types.h>

typedef struct util_ctx *util_ctx_t;

util_ctx_t util_test_setup(const char *test_name);
void util_test_teardown(util_ctx_t);

int util_open(util_ctx_t, const char *path, int flags, mode_t mode);
int util_mkdir(util_ctx_t ctx, const char *path, mode_t mode);

struct util_rss_item {
    const char *guid;
    const char *link;
};

int util_gen_rss(util_ctx_t, const char *path, const struct util_rss_item *);

int util_get_tempdir(util_ctx_t);
