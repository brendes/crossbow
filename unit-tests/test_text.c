/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "unittest.h"
#include "text.h"

#include <string.h>

#if 0
#  include <err.h>
#  define debug(...) warnx(__VA_ARGS__)
#else
#  define debug(...)
#endif

static int test_text_short_boundaries(util_ctx_t ctx, intptr_t opaque)
{
    const char src[] = "abcdef";
    char dst[7];
    const char *result;

    result = text_short(dst, sizeof(dst), src, NULL);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "abcdef") == 0);

    result = text_short(dst, sizeof(dst) - 1, src, NULL);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "ab...") == 0);

    result = text_short(dst, sizeof(dst) - 2, src, NULL);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "a...") == 0);

    result = text_short(dst, sizeof(dst) - 3, src, NULL);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "...") == 0);

    result = text_short(dst, sizeof(dst) - 4, src, NULL);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "..") == 0);

    result = text_short(dst, sizeof(dst) - 5, src, NULL);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, ".") == 0);

    result = text_short(dst, sizeof(dst) - 6, src, NULL);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "") == 0);

    result = text_short(dst, 0, src, NULL);
    EXPECT(result == NULL);

    return 0;
}

static int test_text_join_argv(util_ctx_t ctx, intptr_t opaque)
{
    const char *argv[] = {
        "sed", "-n", "s/foo/bar/p", NULL
    };
    char to_buf[19];
    const char *result;

    result = text_join_argv(to_buf, sizeof(to_buf), argv);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "sed -n s/foo/bar/p") == 0);

    result = text_join_argv(to_buf, sizeof(to_buf) - 1, argv);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "sed -n s/foo/b...") == 0);

    result = text_join_argv(to_buf, sizeof(to_buf) - 8, argv);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "sed -n ...") == 0);

    return 0;
}

static int test_text_join_argv_spaces(util_ctx_t ctx, intptr_t opaque)
{
    const char *argv[] = {
        "sed", "-n", "s/foo /bar/p", NULL
    };
    char to_buf[21];
    const char *result;

    result = text_join_argv(to_buf, sizeof(to_buf), argv);
    debug("-> [%s]", result);
    EXPECT(strcmp(result, "sed -n s/foo\\ /bar/p") == 0);

    return 0;
}

const struct test * list_test(void)
{
    static struct test tests[] = {
        TEST(1, test_text_short_boundaries, NULL),
        TEST(1, test_text_join_argv, NULL),
        TEST(1, test_text_join_argv_spaces, NULL),
        END_TESTS
    };
    return tests;
}
