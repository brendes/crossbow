/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <err.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "launcher.h"

#include "util.h"
#include "unittest.h"

#if 0
#define INFO(...) warnx(__VA_ARGS__)
#else
#define INFO(...)
#endif

static int launcher_base(util_ctx_t ctx, intptr_t opaque)
{
    const char *cmd_true[] = {"/usr/bin/true", NULL};
    const char *cmd_false[] = {"/usr/bin/false", NULL};
    const launcher_run_t run_true = { .argv = cmd_true };
    const launcher_run_t run_false = { .argv = cmd_false };

    int status;

    EXPECT(launcher_execvp(&run_true, &status) == 0);
    EXPECT(status == 0);
    EXPECT(launcher_execvp(&run_false, &status) == 0);
    EXPECT(status != 0);
    EXPECT(launcher_execvp(&run_true, &status) == 0);
    EXPECT(status == 0);
    EXPECT(launcher_execvp(&run_false, &status) == 0);
    EXPECT(status != 0);
    EXPECT(launcher_execvp(&run_true, &status) == 0);
    EXPECT(status == 0);

    return 0;
}

static int launcher_pipe_regular(util_ctx_t ctx, intptr_t opaque)
{
    const char *cmd[] = {"./aux_test_launcher", "-cast", NULL};
    const char text[] = "carasta\ncarasta\n";
    int status;

    EXPECT(launcher_execvp(
        &(launcher_run_t){
            .argv = cmd,
            .input.bytes = (void *)text,
            .input.len = sizeof(text) - 1,
        },
        &status
    ) == 0);

    EXPECT(WIFEXITED(status) && WEXITSTATUS(status) == 10);
    return 0;
}

static int launcher_pipe_long(util_ctx_t ctx, intptr_t opaque)
{
    enum {
        bufsize = 4096 * 2000,  /* hopefully more than pipe buffer: we want to
                                 * block and get write fialure with EPIPE */
    };
    const char *cmd[] = {"./aux_test_launcher", "-s", "10000000", NULL};

    void *buffer;
    int status;

    buffer = malloc(bufsize);
    EXPECT(buffer);
    memset(buffer, 0, bufsize);

    EXPECT(launcher_execvp(
        &(launcher_run_t){
            .argv = cmd,
            .input.bytes = (void *)buffer,
            .input.len = bufsize,
        },
        &status
    ) == 0);

    EXPECT(WIFEXITED(status) && WEXITSTATUS(status) == 0);
    free(buffer);
    return 0;
}

const struct test * list_test(void)
{
    static const struct test tests[] = {
        TEST(1, launcher_base, NULL),
        TEST(1, launcher_pipe_regular, NULL),
        TEST(1, launcher_pipe_long, NULL),
        END_TESTS
    };
    return tests;
}
