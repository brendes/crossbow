/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include "str.h"

#include "util.h"
#include "unittest.h"

static int test_general(util_ctx_t ctx, intptr_t opaque)
{
    const char *ptr1, *ptr3;

    str_free(NULL); /* supported */

    str_t str1 = {};
    EXPECT(str1.bytes == NULL);
    EXPECT(str1.len == 0);
    EXPECT(str1.owned == 0);

    const char *exp = "hello";
    str_t str2 = STR_DEFINE(exp);
    EXPECT(str2.bytes == exp); /* pointer comparison! */
    EXPECT(str2.len = 5);          /* not counting \0 */
    EXPECT(str2.owned == 0);

    EXPECT(str_dup(&str1, &str2) == 0);
    EXPECT(str1.len == str2.len);
    EXPECT(memcmp(str1.bytes, str2.bytes, str1.len) == 0);
    EXPECT(str1.owned == 1);

    str_t str3 = {};
    ptr1 = str1.bytes;
    EXPECT(ptr1 != NULL);
    EXPECT(str_dup(&str3, &str1) == 0);
    ptr3 = str3.bytes;
    EXPECT(ptr3 != NULL);
    EXPECT(ptr3 != ptr1);
    EXPECT(memcmp(ptr1, ptr3, str3.len) == 0);
    str_move(&str3, &str1);
    EXPECT(str3.bytes == ptr1);
    EXPECT(str1.bytes == str3.bytes);
    EXPECT(str1.len == str3.len);
    EXPECT(str1.owned == 0);

    str_free(&str3);
    EXPECT(str3.bytes == NULL);
    EXPECT(str3.len == 0);
    EXPECT(str3.owned == 0);

    return 0;
}

static int test_move(util_ctx_t ctx, intptr_t opaque)
{
    str_t str1 = STR_DEFINE("hello");
    str_t str2 = {};
    str_t str3 = {};

    EXPECT(str_dup(&str2, &str1) == 0);
    EXPECT(!str1.owned);
    EXPECT(str2.owned);

    str_move(&str3, &str1);
    EXPECT(!str1.owned);
    EXPECT(!str3.owned);

    str_move(&str3, &str2);
    EXPECT(!str2.owned);
    EXPECT(str3.owned);

    str_free(&str3);
    return 0;
}

const struct test * list_test(void)
{
    static struct test tests[] = {
        TEST(1, test_general, NULL),
        TEST(1, test_move, NULL),
        END_TESTS
    };
    return tests;
}
