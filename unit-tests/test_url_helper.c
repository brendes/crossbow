/*
 * This file is part of Crossbow.
 *
 * Crossbow is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Crossbow is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Crossbow.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <unistd.h>

#include "str.h"
#include "url_helper.h"

#include "util.h"
#include "unittest.h"

#if 0
# include <err.h>
# define debug(...) warnx(__VA_ARGS__)
#else
# define debug(...)
#endif

typedef struct {
    url_pair_t query;
    url_pair_t exp_answer;
    bool exp_pwd_prefix;
} test_case_t;

static int expand_path(char *buf, const test_case_t *tc, size_t len)
{
    const str_t *url;
    size_t j = 0;

    url = &tc->exp_answer.url;
    if (!url->bytes)    /* if exp_answer.url is undef, we expect it not to change */
        url = &tc->query.url;

    if (tc->exp_pwd_prefix) {
        EXPECT(getcwd(buf, len) != NULL);
        j += strlen(buf);
        buf[j++] = '/';
    }

    for (int i = 0; i < url->len && j < len - 1; ++i, ++j)
        buf[j] = url->bytes[i];
    buf[j] = '\0';

    return 0;
}

static int test_get_effective(util_ctx_t ctx, intptr_t opaque)
{
    const test_case_t *tc = (const test_case_t *)opaque;
    url_pair_t answer;
    char e_path[PATH_MAX];

    EXPECT(expand_path(e_path, tc, PATH_MAX) == 0);
    EXPECT(url_get_effective(&tc->query, &answer) == 0);

    debug("compare effective %d vs expected %d",
        answer.type,
        tc->exp_answer.type);
    EXPECT(answer.type == tc->exp_answer.type);
    debug("compare effective %.*s vs expected %s",
        STR_FMT(&answer.url),
        e_path);
    EXPECT(
        str_cmp(
            &answer.url,
            &(str_t){
                .bytes = e_path,
                .len = strlen(e_path),
            }
        ) == 0
    );
    str_free(&answer.url);

    return 0;
}

const struct test * list_test(void)
{
    static test_case_t detect_cases[] = {
        [0].query.url = STR_DEFINE("http://example.org/feed.rss"),
        [0].query.type = feed_ut_unknown,
        [0].exp_answer.type = feed_ut_remote,

        [1].query.url = STR_DEFINE("https://example.org/feed.rss"),
        [1].query.type = feed_ut_unknown,
        [1].exp_answer.type = feed_ut_remote,

        [2].query.url = STR_DEFINE("example.org/feed.rss"),
        [2].query.type = feed_ut_unknown,
        [2].exp_answer.url = STR_DEFINE("https://example.org/feed.rss"),
        [2].exp_answer.type = feed_ut_remote,

        [3].query.url = STR_DEFINE("file:///example.org/feed.rss"),
        [3].query.type = feed_ut_unknown,
        [3].exp_answer.url = STR_DEFINE("/example.org/feed.rss"),
        [3].exp_answer.type = feed_ut_local,

        [4].query.url = STR_DEFINE("file://example.org/feed.rss"),
        [4].query.type = feed_ut_unknown,
        [4].exp_answer.url = STR_DEFINE("example.org/feed.rss"),
        [4].exp_answer.type = feed_ut_local,
        [4].exp_pwd_prefix = true,

        /* Similar to test [2], but the initial '/' will suggest that we
         * are talking of a local file. */
        [5].query.url = STR_DEFINE("/example.org/feed.rss"),
        [5].query.type = feed_ut_unknown,
        [5].exp_answer.type = feed_ut_local,

        [6].query.url = STR_DEFINE("./example.org/feed.rss"),
        [6].query.type = feed_ut_unknown,
        [6].exp_answer.type = feed_ut_local,
        [6].exp_pwd_prefix = true,

        [7].query.url = STR_DEFINE("../example.org/feed.rss"),
        [7].query.type = feed_ut_unknown,
        [7].exp_answer.type = feed_ut_local,
        [7].exp_pwd_prefix = true,

        [8].query.url = STR_DEFINE("gopher://example.org/feed.rss"),
        [8].query.type = feed_ut_unknown,
        [8].exp_answer.type = feed_ut_remote,
    };

    static test_case_t remote_cases[] = {
        [0].query.url = STR_DEFINE("http://example.org/feed.rss"),
        [0].query.type = feed_ut_remote,
        [0].exp_answer.type = feed_ut_remote,

        [1].query.url = STR_DEFINE("https://example.org/feed.rss"),
        [1].query.type = feed_ut_remote,
        [1].exp_answer.type = feed_ut_remote,

        [2].query.url = STR_DEFINE("example.org/feed.rss"),
        [2].query.type = feed_ut_remote,
        [2].exp_answer.url = STR_DEFINE("https://example.org/feed.rss"),
        [2].exp_answer.type = feed_ut_remote,

        /* this is quite stupid, but if the user forces it to be remote we
         * just obey. */
        [3].query.url = STR_DEFINE("file:///example.org/feed.rss"),
        [3].query.type = feed_ut_remote,
        [3].exp_answer.url = STR_DEFINE("https://file:///example.org/feed.rss"),
        [3].exp_answer.type = feed_ut_remote,

        [4].query.url = STR_DEFINE("gopher://example.org/feed.rss"),
        [4].query.type = feed_ut_remote,
        [4].exp_answer.type = feed_ut_remote,
    };

    /* This group is on average even more stupid.  But if the user forces
     * a local file, it must be respected. */
    static test_case_t local_cases[] = {
        [0].query.url = STR_DEFINE("http://example.org/feed.rss"),
        [0].query.type = feed_ut_local,
        [0].exp_answer.type = feed_ut_local,
        [0].exp_pwd_prefix = true,

        [1].query.url = STR_DEFINE("https://example.org/feed.rss"),
        [1].query.type = feed_ut_local,
        [1].exp_answer.type = feed_ut_local,
        [1].exp_pwd_prefix = true,

        [2].query.url = STR_DEFINE("example.org/feed.rss"),
        [2].query.type = feed_ut_local,
        [2].exp_answer.type = feed_ut_local,
        [2].exp_pwd_prefix = true,

        [3].query.url = STR_DEFINE("file:///example.org/feed.rss"),
        [3].query.type = feed_ut_local,
        [3].exp_answer.url = STR_DEFINE("/example.org/feed.rss"),
        [3].exp_answer.type = feed_ut_local,

        [4].query.url = STR_DEFINE("gopher://example.org/feed.rss"),
        [4].query.type = feed_ut_local,
        [4].exp_answer.type = feed_ut_local,
        [4].exp_pwd_prefix = true,
    };

    static struct test tests[] = {
        TEST(1, test_get_effective, &detect_cases[0]),
        TEST(1, test_get_effective, &detect_cases[1]),
        TEST(1, test_get_effective, &detect_cases[2]),
        TEST(1, test_get_effective, &detect_cases[3]),
        TEST(1, test_get_effective, &detect_cases[4]),
        TEST(1, test_get_effective, &detect_cases[5]),
        TEST(1, test_get_effective, &detect_cases[6]),
        TEST(1, test_get_effective, &detect_cases[7]),
        TEST(1, test_get_effective, &detect_cases[8]),

        TEST(1, test_get_effective, &remote_cases[0]),
        TEST(1, test_get_effective, &remote_cases[1]),
        TEST(1, test_get_effective, &remote_cases[2]),
        TEST(1, test_get_effective, &remote_cases[3]),
        TEST(1, test_get_effective, &remote_cases[4]),

        TEST(1, test_get_effective, &local_cases[0]),
        TEST(1, test_get_effective, &local_cases[1]),
        TEST(1, test_get_effective, &local_cases[2]),
        TEST(1, test_get_effective, &local_cases[3]),
        TEST(1, test_get_effective, &local_cases[4]),
        END_TESTS
    };
    return tests;
}
