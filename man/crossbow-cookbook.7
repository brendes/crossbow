.Dd July 11, 2020
.Dt CROSSBOW 7 URM
.Os \&
.\" ---------------------------------------------------------------------
.Sh NAME
.Nm crossbow-cookbook
.Nd examples of handling feeds
.\" ---------------------------------------------------------------------
.Sh SYNOPSIS
.Nm crossbow set
.Op ...
.\" ---------------------------------------------------------------------
.Sh DESCRIPTION
This manual page contains short recipes describing common usage
patterns for the
.Nm crossbow
feed aggregator.
.Pp
In all the following examples we will assume that the
.Ev $ID
environment variable is defined as an arbitrary feed identifier, and that
the
.Ev $URL
environment variable is defined as the feed URL.
.\" ---------------------------------------------------------------------
.Sh EXAMPLES
.\" --- example
.Ss Simple local mail notification
We want a periodic bulk notification about updates availability.
.Pp
The following feed set up can be used for this purpose:
.Bd -literal -offset XXXXXX
crossbow set -i "$ID" -u "$URL" \e
  -o pretty \e
  -f "updates from $ID:\en title: %t\en link: %l\en"
.Ed
.Pp
The invocation of
.Xr crossbow-fetch 1
will emit on
.Xr stdout 3
a "record" like the following for each new item:
.Bd -literal -offset XXXXXX
updates from foobar:
 title: Today is a good day
 link: http://example.com/today-is-a-good-day
.Ed
.Pp
The user can schedule on
.Xr cron 8
a periodic invocation of
.Xr crossbow-fetch 1 .
Assuming that local mail delivery is enabled, and since any output of a
cronjob is emailed to the owner of the
.Xr crontab 5 ,
the user will receive an email having as body the concatenation of
the records.
.\" --- example
.Ss Keep a local HTML file collection
Let's consider the case of a feed for which the item's
.Sy description
field reports the whole article in HTML format.
Individual articles need to be stored in a separate HTML file under a
certain directory on the filesystem.
.Pp
The following feed set up can be used for this purpose:
.Bd -literal -offset XXXXXX
crossbow set -i "$ID" -u "$URL" \e
  -o pipe \e
  -f "sed -n w%n.html" \e
  -C /some/destination/path/
.Ed
.Pp
The invocation of
.Xr crossbow-fetch 1
will spawn one
.Xr sed 1
process for each new item.
The item description will be piped to
.Xr sed 1 ,
which in turn will write it on a file
.Po
.Sy w
command
.Pc .
The output files will be named
.Sy 000000.html ,
.Sy 000001.html ,
.Sy 000002.html ... ,
since
.Sy %n
is expanded with an incremental numeric value.
See
.Xr crossbow-outfmt 5 .
.Pp
.Sy Security remark :
Unless the feed is
.Em trusted ,
it is strongly discouraged to use anything but
.Sy %n
to name files.
Consider for example the case where
.Sy %t
is used instead of
.Sy %n ,
and the title of a post is
.Sy ../../../../home/user/public_html/index
.Pp
.Sy Security remark :
We are using the
.Sy w
command of
.Xr sed 1
to write to a file.
It is not possible to use shell redirection since sub-commands are never
executed through a shell interpreter.
Invoking a shell interpreter from a command template is strongly
discouraged, since the placeholders would be directly mixed with the shell
script, and doing proper shell escaping against untrusted input is really
hard, if not impossible.
It is on the other hand safe to invoke a shell script whose code lives
in a file and pass parameters to it.
See
.Xr crossbow-outfmt 5 .
.\" --- example
.Ss Download the full article
This scenario is similar to the previous one, except that the item
.Sy description
contains only part of the content, or nothing at all.
The
.Sy link
field contains a valid URL, which is intended to be reached by
means of a browser.
.Pp
In this case we can leverage
.Xr curl 1
to do the retrieval:
.Bd -literal -offset XXXXXX
crossbow set -i "$ID" -u "$URL" \e
  -o subproc \e
  -f "curl -o %n.html %l"
  -C /some/destination/path/
.Ed
.Pp
.Sy Remark :
Placeholders such as
.Sy %n
and
.Sy %l
do not need to be quoted: they are handled safely even when their
expansions contain whitespaces.
.\" --- example
.Ss One mail per item
We want to turn individual feed items into plain (HTML-free) text messages
delivered via email.
.Pp
Our goal can be achieved by means of a generic shell script like
the following:
.Bd -literal -offset XXXXXX
#!/bin/sh

set -e

feed_title="$1"
post_title="$2"
link="$3"

lynx "${link:--stdin}" -dump -force_html |
    sed "s/^~/~~/" |    # Escape dangerous tilde expressions
    mail -s "${feed_title:+${feed_title}: }${post_title:-...}" "${USER:?}"
.Ed
.Pp
The script can be installed in the
.Ev PATH ,
e.g. as
.Sy /usr/local/bin/crossbow-to-mail ,
and then integrated in
.Xr crossbow 1
as follows:
.Bl -bullet
.It
If the feed provides the whole content as item
.Sy description :
.Bd -literal -offset XXXXXX
crossbow set -i "$ID" -u "$URL" \e
    -o pipe \e
    -f "crossbow-to-mail %ft %t"
.Ed
.It
If the feed provides only the URL of the article as item
.Sy link :
.Bd -literal -offset XXXXXX
crossbow set -i "$ID" -u "$URL" \e
    -o subproc \e
    -f "crossbow-to-mail %ft %t %l"
.Ed
.El
.Pp
.Sy Remark :
The
.Sy crossbow-to-mail
script depends on the excellent
.Xr lynx 1
browser to download and parse the HTML into textual form.
.Pp
.Sy Security remark :
The
.Qq s/^~/~~/
.Xr sed 1
command prevents
.Sy tilde escapes
to be honored by unsafe implementations of
.Xr mail 1 .
The
.Xr mutt 1
mail user agent, if available, can be used as a safer drop-in replacement.
.\" --- example
.Ss Follow YouTube user, channel or playlist
The YouTube site provides feeds for users, channels and playlists.
Each of these entities is assigned with a unique identifier which can be
easily obtained by looking at the web URL.
.Pp
Once the
.Ar user ,
.Ar channel
or
.Ar playlist
identifier is known, it is trivial to obtain the corresponding feeds:
.Bl -bullet
.It
.Sy https://youtube.com/feeds/videos.xml?user= Ns Ar user
.It
.Sy https://youtube.com/feeds/videos.xml?channel_id= Ns Ar channel
.It
.Sy https://youtube.com/feeds/videos.xml?playlist_id= Ns Ar playlist
.El
.Pp
It is possible to combine
.Xr crossbow 1
with the
.Xr youtube-dl 1
tool, to maintain up to date a local collection of video or audio files.
.Pp
What follows is a convenient wrapper script that ensures proper file
naming:
.Bd -literal -offset XXXXXX
#!/bin/sh

link="${1:?mandatory argument missing: link}"
incremental_id="${2:?mandatory argument missing: incremental id}"
format="$3"

# Transform a title in a reasonably safe 'slug'
slugify() {
    tr -d \e\en |                     # explicitly drop new-lines
    tr /[:punct:][:space:] . |      # turn all sneaky chars into dots
    tr -cs [:alnum:]                # squeeze ugly repetitions
}

fname="$(
    youtube-dl \e
        --get-filename \e
        -o "%(id)s_%(title)s.%(ext)s" \e
        "$link"
)" || exit 1

youtube-dl \e
    ${format:+-f "$format"} \e
    -o "$(printf %s_%s "$incremental_id" "$fname" | slugify)" \e
    --no-progress \e
    "$link"
.Ed
.Pp
Once again, the script can be installed in the
.Ev PATH ,
e.g. as
.Sy /usr/local/bin/crossbow-ytdl
And then integrated in
.Xr crossbow 1
as follows:
.Bl -bullet
.It
To save each published item:
.Bd -literal -offset XXXXXX
crossbow set -i "$ID" -u "$URL" \e
    -o subproc \e
    -f "crossbow-ytdl %l %n" \e
    -C /some/destination/path
.Ed
.It
To save each published item as audio:
.Bd -literal -offset XXXXXX
crossbow set -i "$ID" -u "$URL" \e
    -o subproc \e
    -f "crossbow-ytdl %l %n bestaudio" \e
    -C /some/destination/path
.Ed
.El
.\" ---------------------------------------------------------------------
.Sh SEE ALSO
.Xr crossbow-fetch 1 ,
.Xr crossbow-set 1 ,
.Xr lynx 1 ,
.Xr sed 1 ,
.Xr youtube-dl 1 ,
.Xr crontab 5 ,
.Xr cron 8
.\" ---------------------------------------------------------------------
.Sh AUTHORS
.An Giovanni Simoni Aq Mt dacav@fastmail.com
