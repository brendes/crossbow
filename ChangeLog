2020-09-03, VERSION 1.1.3

 Add user-agent ("crossbow/$version") - contrib. Matt Carroll

 Minor man-pages improvements

2020-08-04, VERSION 1.1.2

 Dropped crossbow-outfmt(1), which was a noinst_PROGRAMS anyway

 Fixed compilation under Darwin

 Use pledge(2) under OpenBSD - contrib. Anton Lindqvist <anton@basename.se>

 General improvement and bug fixes

2020-07-05, VERSION 1.1.1

 Extra placeholders %ft (feed title) and %fi (feed identifier)

 The crossbow-fetch(1) command supports dry-run modes (-d and -D) and a
 catch-up mode (-c).

 Man pages refinements

 Other general improvements and bug fixes

2020-06-12, VERSION 1.0.1

 Fixed freebsd compilation, accidentally neglected

2020-06-12, VERSION 1.0.0

 New storage format

    The new storage format is incompatible with the 0.9.0 format, but it
    is way easier to have backward and forward compatible formats now.

    The ~/.crossbow directory is populated by one file for each registered
    feed.

 Subprocess failure handling: feed items whose processing failed are not
 marked as seen

    The item processing might fail, and we consider the item as not seen.

 URL type auto-detection

    crossbow-set(1) gives an interpretation of the feed URL.  E.g. if the
    url is example.com/feed.rss it will add https:// in front of it.  If
    the url starts with / it is interpreted as a local filesystem path.

    The command flags allow to force a specific interpretation.

 Renamed crossbow-list(1) as crossbow-query(1), and added a couple of
 command line options

 Introduced %n placeholder

    %n provides an incrmeental feed item identifier.  This is a
    security improvement, more details in the man pages.

 Introduced -C

    Execution of subtask in a different directory, named after make's
    -C flag.

 Improved man pages, introduced crossbow-cookbook(7)

 More testing, bug fixes, and general improvements

2020-03-25, VERSION 0.9.0

 First release
